## 下载 wps
[下载](http://linux.wps.cn/)  

```bash
sudo dpkg -i program_name.deb

// 如果出错
sudo apt-get install -f
```


## 解决 wps 字体缺失问题
在备份文件中下载字体包  

```bash
sudo unzip wps_symbol_fonts.zip
sudo cp mtextra.ttf  symbol.ttf  WEBDINGS.TTF  wingding.ttf  WINGDNG2.ttf  WINGDNG3.ttf  /usr/share/fonts
```
然后解压后的字体可以删除了
