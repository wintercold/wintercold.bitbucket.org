## 安装搜狗输入法
1. 64位linux  
[http://10.182.80.11/cache/11/01/cdn2.ime.sogou.com/a1b78ad374f269e758526a230270991b/sogoupinyin_2.2.0.0102_amd64.deb?st=Yy10RkLzjGfU7wdVx33Wgw&e=1520948215&fn=sogoupinyin_2.2.0.0102_amd64.deb](http://10.182.80.11/cache/11/01/cdn2.ime.sogou.com/a1b78ad374f269e758526a230270991b/sogoupinyin_2.2.0.0102_amd64.deb?st=Yy10RkLzjGfU7wdVx33Wgw&e=1520948215&fn=sogoupinyin_2.2.0.0102_amd64.deb)  

2. 安装
```bash
sudo dpkg -i sogoupinyin_2.2.0.0102_amd64.deb 
sudo apt-get install -f 
```

3. 配置  
  - 配置System settings
进入System Settings > Language Support >  Keyboard input method system  
将值改为**fcitx**  然后注销重启  

  - 在bash中打开 fcitx Configure
Input Method 下添加`sogou Pinyin`

## 图标设置为双击最小化

