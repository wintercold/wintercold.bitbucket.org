> 隐藏对象的信息 ,留出访问的接口  

## 一.实现封装类
> 1.封装类 2.留出访问接口(get/set) 3. 调用

![](../assets/fengzhuang.png)  

### 1. 对类进行封装

```java
public class Cat {
    // 成员属性
    private String name;//将name属性设为私有
    int age;
    double weight;
    String specie;

    // 无参构造方法
    public Cat(){

    }

    // 有参构造方法
    public Cat(String name,int age,double weight,String specie){
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.specie = specie;
    }

    // set方法获得对象传递的方法
    public void setName(String name){
        this.name = name;

    }

    // get方法向对象返回逻辑处理后的结果
    public String getName(){

        return this.name;
    }
}
```

### 2. 对象通过get/set方法访问类

```java
public class CatInit {
    public static void main(String args[]){
        //实例化对象
        Cat katty = new Cat();

        // 把Tom这个name属性传到Cat类中
       katty.setName("Tom");

       // 从Cat类中得到返回值
       System.out.print(katty.getName());
    }
}
```

### 3. 有参构造方法调用set方法
我们封装了Cat类，在set方法中添加一段逻辑代码，检查输入的参数是否符合逻辑（如将年龄设为负数）；  

在有参构造函数中，我们应该在构造函数中调用set方法，即在`this.name=name`前调用set方法检查参数是否符合逻辑

## 包进行类管理
包对类的管理，就像文件夹对文件的管理，同一个包中不能有同名的类，同名的类可以在不同的包中。 
 
那么，怎样跨包访问类呢？
  
```java
//定义包
package com.easter.zoo
//1.导入包(包名+类名)
import com.easter.zoo.animal;
//2.导入包（在new时写绝对路径）
com.easter.zoo.animal.Cat tom = new com.easter.zoo.animal.Cat();
```

## 二. static关键字
> 

```java
public class Cat {
    static String name;  // 类变量
    int age;             // 成员变量
    
    void eat(){
        String food = "";        // 局部变量
        System.out.println("eat" + food);
    }
}
```

### 1. static (类成员)的特性
> 利: 对对象的共享区域进行单独空间存储,节省空间,没必要每个对象都存储一遍
弊: 生命周期过长 && 访问局限性

1. 随着类的加载而加载,随着类的消失而消失,说明他的生命周期最长
2. 优先于对象存在
3. 被所有对象共享, 是所有对象的共性
4. 推荐用类名调用,因为用对象调用,该对象可能还没被创建
5. 成员方法可以直接访问静态变量, 静态方法不能直接访问成员方法(可以通过new一个对象，再通过对象去调用非静态方法和属性。)
6. 定义过多的类变量, 生命周期过长, 反而造成内存紧张
7. static 只能修饰成员变量,将成员变量转为类变量,不能修饰类和局部变量
8. 静态方法中不可以写this,super关键字

### 2. static 的使用
> 类成员, 工具类, 静态代码块,主函数

###### 1. 类成员
- 类变量的使用场景  
当对象中出现共享数据时,该数据被静态修饰;  
对象中的特有属性要定义成成员变量.  

- 类方法的使用场景  
当功能内部不需要访问非静态成员时,可以将改方法定义成静态方法.  


###### 2. 工具类
> 一个严谨的工具类: public 修饰的类 + 所有方法都为 static 静态 + 私有的构造方法 + 私有的内部调用方法  

[Array工具类](https://github.com/EasterFan/JavaExercise/blob/master/basic/src/array/ArrayTool.java)  
凡是 public 修饰的都可以被文档注释器识别(`javadoc -d . -author -version ArrayTool.java`).  

###### 3. 静态代码块
随着类的加载而执行,只执行一次,  
用于**给类进行初始化**

###### 4. 主函数
> 主函数是固定格式的(只有args可改),作为程序的入口,被 JVM 直接调用


JVM 在调用主函数时, 传入的是 new String[0];
```java
	public static void main(String[] args) {
        System.out.println(args);  // [Ljava.lang.String;@61bbe9ba
        System.out.println(args.length);  // 0
    }
```
