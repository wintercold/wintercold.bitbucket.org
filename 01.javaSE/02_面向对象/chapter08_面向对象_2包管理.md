> 包进行类管理

## 包访问
![](../assets/packagepermission.png)  
包与包之间相互访问, 被访问的包中的类以及类中的成员, 需要 public 修饰.   

## Jar 包
> jar.exe 制作 java 的压缩包,

- 创建jar包  
`jar -cvf mypack.jar [包名1] [包名2]`
- 查看jar包 -- 将jar 包显示信息重定向到其他文件中  
`jar -tvf mypack.jar [>定向文件]`
- 解压缩
`jar -xvf mypack.jar`
- 自定义jar包的清单文件
`jar –cvfm mypack.jar mf.txt [包名1] [包名2]`
