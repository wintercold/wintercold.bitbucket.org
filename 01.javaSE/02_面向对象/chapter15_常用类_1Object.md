> Object 类是所有类的父类

## 一. Object 类的特点
1. 继承中所有类都直接/间接继承 Object 类,  
2. 所有子类默认构造方法中第一行都是`super()`,Object的构造方法中没有`super()`  

## 二. Object 类方法
#### 1. euqals()  
1. java中认为所有对象都具有可比性,equals 比较的是复合数据类型的内存地址  
2. 对于基本数据类型,`==`比较的是值  
对于复合数据类型,`==`和`equals`比较的是堆内存地址.  
3. **String,Integer,Date在这些类当中equals被重写,有其自身的实现，而不再是比较类在堆内存中的存放地址了。**  
4. `equals(Object obj)`本身是多态的应用, obj 是父类,使用的时候要向下转型.  
[多态 -- Object向下转型](https://github.com/EasterFan/JavaExercise/blob/master/PolyProj/src/com/easter/mytest.java)  

#### 2. toString()和hashCode()  
1. java认为所有对象都可转为字符串(即哈希值)打印  
2. toString打印的是十六进制的内存地址, 父类中这样打印没有意义,很多类都重写 toString   
3. hashCode打印的是十进制的内存地址  

```java
	MyDemo d1 = new MyDemo(4);
	System.out.println(d1.toString());
        System.out.println(Integer.toHexString(d1.hashCode()));
```

#### 3. getClass()  
> 对象都是依赖于类文件产生的, 这些类文件(编译后的class文件)都有名称,构造函数,一般方法,所以用 **Class 类**来描述这些class文件.  

1. getClass方法用于获得该类编译后的 class 文件.  
2. 通过编译后的class文件得到对象中的变量,方法,构造函数等, 这个过程就叫做java的**反编译**  
3. 通过 getClass 拿到**.java文件**编译后的**.class文件**后,可以做什么,参见 API 中的Class类  
