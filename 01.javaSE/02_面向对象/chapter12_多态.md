## 一. 多态的概念
> 某一类事物有多种存在形态(人分为男人和女人 -- 你称呼卓别林人或男人都可)  

多态可以分为**编译时多态**和**运行时多态**，前者表示编译器在编译时即可进行不同行为的区分，java中的多态是后者。  

```java
男人 卓别林 = new 男人();
人 卓别林 = new 男人();
```
函数的重载和重写就可以看作是**函数的多态** -- 同一函数名对应多种方法体.  
java 中主要研究**对象的多态**  

## 二. 多态的特点
1. 体现  
父类或者接口的引用指向或者接收自己的子类对象。  
多态自始至终都是子类对象在做变化---向上向下转型  

2. 作用:  
多态的存在提高了程序的扩展性和后期可维护性  
简化了对象调用 -- 找到一批对象的共同特性,统一操作很多对象.  

3. 前提:  
  - 需要存在继承或者实现关系  
  - 要有覆盖操作  

4. 弊端:  
提高了扩展性,但是只能用父类引用去访问父类成员.  

5. 运行特点  
**成员函数**: -- 编译看父类,运行看子类  
  - 编译时:要查看引用变量所属的类(父类)中是否有所调用的成员。  
  - 在运行时:要查看对象所属的类(子类)中是否有所调用的成员。  
**成员变量和静态成员(static)**: -- 编译和运行都看父类  
  - 只看引用变量所属的类(父类)  

## 三. 多态的应用

### 1. 向上转型
向上转型，小类转大类  

```java
  // 向上转型：把子类对象赋给父类引用
        Animal cat_up = new Cat();
        Animal dog_up = new Dog();
```
向上转型后的animal_cat可以调用自己重写父类的方法，可是不能再调用以前自己定义的专属的方法了。  

### 2. 向下转型
向下转型，大类转小类（需强转）  

```java
// 向下转型，大类转小类，向上转型的还原
		Cat cat_down = (Cat) cat_up;
		cat_down.eat();
		cat_down.run();
```
注意，大类转小类，是还原，这个**大类**本来就是从小类转成的**大类**，由Animal类实例化的animal是不能向下转型为小类的。  

另外，向下转型，恢复成小类后，这个小类又可以调用自己原来的专属方法了。  

### 3. instanceof 安全向下转型
要向下转型，我们首先要知道要转型的这个大类，他的前身是哪一个小类，instanceof方法就是判断这个转型的大类是否属于待转型的小类。返回值为true/false。我们要做的，就是在向下转型前，加上一段判断逻辑。  

```java
if(cat_up instanceof Cat){
            Cat cat_down = (Cat) cat_up;
            cat_down.eat();
            cat_down.run();
            System.out.print("cat_up可以向下转型为Cat\n");
        }

```

[instancof安全转型](https://github.com/EasterFan/JavaExercise/blob/master/PolyProj/src/com/easter/test/PolyTest.java)  


### 4. 多态用于扩展程序
> 在主程序中写死了数据库的连接方式,会导致更改框架时(JDBC转为Hibernate),大量改代码 -- 以数据库连接的 DAO 模式为例.  

```java
// 多态的写法 -- 通过 Dao 层对两种连接方式解耦
public class PolyInJDBC {
    public static void main(String[] args) {
        // 假设使用 JDBC
        UserInfoDao ui = new GetUserInfoByJDBC();

        // 假设使用 Hibernate
        UserInfoDao ui2 = new GetUserInfoByHibernate();

    }
}
```

原来将 JDBC 连接方式写在主程序中, 换用 Hibernate 后需要大量改代码, 不能兼容其他连接方式,程序扩展性很差.  

通过引入 JDBCDAO 接口层后, **将主程序和连接方式之间进行解耦**,再加上反射,可以在不改动代码的情况下,兼容多种连接方式,从而增强程序扩展性.  
[多态用于程序扩展 -- 数据库连接的DAO设计]()

