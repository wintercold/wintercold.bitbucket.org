> 当抽象类中所有方法都是抽象方法时, 我们把这个抽象类定义成**接口**

接口弥补了java单继承的遗憾，接口实现了**行为的关联**，解决了两个问题：  
1. 一个类中需兼容多种类特征的问题  
如手机类继承电脑，相机，智能手表的功能-----但java不允许多继承  

2. 多个不同类具有相同特征  
手机和相机都有拍照功能  

## 一. 接口的特点

### 1. 基本特点
1. 接口的成员(常量,方法)修饰符是固定的 - 常量:`public static final`方法:`public abstract`
2. 接口是对外暴露的规则(都是public)
3. 接口**不可以**创建对象, 因为有抽象方法需要被子类实现,子类对接口中的抽象方法**全部**重写后,**子类**才可以实例化
4. 接口是程序的功能扩展(扩展的是体系外的功能,如果是体系内的共性功能,直接封装成内部属性)
5. **类与接口**之间是实现关系,而且类可以继承一个类的同时实现多个接口
6. 接口与接口之间有继承关系,且**接口和接口之间可以多继承**  
7. 根据**问题领域**的不同,确定哪些行为可以定义为体系外的接口,**基本功能定义在类中,扩展功能定义在接口中**  
```java
// 学生类 StudentA 的主要功能是学生, 扩展功能是工作
class StudentA extends Student implements PartTimeJob{
}
```

### 2. 概念对比
1. java 为什么不支持多继承,支持多实现??  
多继承中,父类的方法名有重复,调用会出现混乱；   
多实现的方法, 只有方法名, 没有方法体,子类继承后可以任意完善方法体,所以父类方法重名也没有关系.  

2. 继承和接口实现的区别?  
  - 继承是子类**直接**拿父类的方法用,接口实现是子类将父类的方法体全部实现后,子类才能实例化调用父类的方法  
  - 类和类之间的关系是继承,接口和接口之间是继承,类和接口之间是实现.  
  - 写方法体是实现, 不写方法体是继承.  
  - 继承条件**A is a B**,接口条件**A like B**(强调行为的相似)

3. 抽象类不能被实例化,那么,抽象类有没有构造方法?  
抽象类没有实例价值, 抽象类有继承价值, 子类实例化时, 先调用父类的默认构造方法,故抽象类有构造方法.  

1.**接口类：**没有方法体，使用该接口的类必须重写接口方法  
```java
public interface InterfaceBody {
    // 成员常量
    public static final String MY_XYZ = "xyz";

    // 成员方法
    public abstract void eat();
}
```

2.**接口的使用：**implement关键字，重写接口的**所有方法**  
```java
public class Android implements IPhoto {

    @Override
    public void photo() {
        System.out.println("我是Android手机的拍照功能");
    }
}
```
如果你不想重写接口的所有方法，就要把自己变成抽象类，你不重写，让你的子类来重写

3.**接口测试：**接口对象虽然由Android等实例化产生，但接口对象只能调用Android类中重写接口的方法，不能调用Android的其他方法  

```java

// 接口引用指向Android/Camara等使用接口的类
        IPhoto ip_Android = new Android();
// 接口对象只能调用接口中的方法，不能调用Android中的其他方法
        ip_Android.photo();
```

[接口的实现](https://github.com/EasterFan/JavaExercise/blob/master/interfaceProj/src/com/easter/test/_01AchievementTest.java)

## 二. 接口成员
一个接口类由五部分组成：抽象方法，常量，默认方法，静态方法，接口实例。  
默认方法和静态方法是jdk1.8以后才支持的。  
[接口成员](https://github.com/EasterFan/JavaExercise/blob/master/interfaceProj/src/com/easter/test/_02Member.java)  

#### 1. 抽象方法
接口类中默认方法都是抽象方法，自动为方法加上修饰符`public abstract`  
```java
//接口中默认的方法都是抽象方法
void network();

// 默认等价于
public abstract void network();
```
#### 2. 常量
接口类中默认成员变量都是常量，自动为成员变量加上修饰符`public static final`  

```java
//接口中默认的成员变量都是常量
int temp = 1;

// 默认等价于
public static final temp = 1;
```
常量的两种调用方法：  

```java
// 1. 通过接口名调用
		IPhoto.temp
// 2. 通过接口实例调用
		IPhoto ip_Android = new Android();
		ip_Android.temp;
```
#### 3. 默认方法
我们不想重写接口中的所有方法，也不想把自己变成抽象类，有没有第三种解决方法呢？   
我们把不想重写的方法定义成默认方法，就不用重写他了。  

```java
// default 默认方法 可以带方法体
		default void network(){
			System.out.print("我是接口中的默认方法");
		}
```
默认方法可以通过接口实例调用。  
或者通过接口名调用：`INet.super.network();`--测试报错  
默认方法是可以被重写的。  
#### 4. 静态方法

```java
// static 静态方法 可以带方法体
		static void stop(){
			System.out.print("我是接口中的静态方法");
		}
```
静态方法只能通过接口调用。不能通过接口实例调用。  
静态方法可以被继承，不可以被重写。  

#### 5. 接口实例

```java
          IPhoto ip_Android = new Android();
```
ip_Android就是一个接口实例，  
ip_Android调用的方法是接口中定义的抽象方法，  
ip_Android调用的static常量是接口中的常量  

## 三. 多接口重名问题
java中一个类可以实现多个接口，有一个问题是，一个类实现的两个接口中有同名的**默认方法**，或者有同名的**常量**，此时这个类就会报错，怎样解决？  
1. 默认方法重名  
解决方法：  
在这个类中重写这个同名的默认方法，这样，用接口实例调用该方法时，会调用这个类中重写的方法。   
如果一个实现类，他的父类和接口中有重名的方法，用接口实例调用该方法时，会优先调用父类的方法。   
总结，默认同名方法调用优先级：  
> 子类同名方法 > 父类同名方法 > 接口同名方法  

2. 常量重名
当两个接口中有相同的常量时，我们在调用的时候通过`接口名+常量`的方法进行调用即可。  
当两个接口和父类中的常量同名时，父类常量不占优势了，还是需要在实现类中重新定义这个常量。  

[多接口重名问题](https://github.com/EasterFan/JavaExercise/blob/master/interfaceProj/src/com/easter/test/_03Duplication.java)  

## 四. 接口的继承
接口之间可以实现继承关系，并且一个接口可以继承多个父接口，这一点比较特殊。  
实现这个子接口的实现类，当然就需要重写该子接口和其所有父接口的抽象方法。  

```java
interface A {
    void methodA();
}

interface B {
    void methodB();
}

// 接口的多继承
interface C extends A,B{
    void methodC();
}

// 实现这个接口,就要重写该接口的所有方法
class D implements C{

    @Override
    public void methodA() {
        
    }

    @Override
    public void methodB() {

    }

    @Override
    public void methodC() {

    }
}

```
[接口继承](https://github.com/EasterFan/JavaExercise/blob/master/interfaceProj/src/com/easter/test/_04Inherance.java)  


