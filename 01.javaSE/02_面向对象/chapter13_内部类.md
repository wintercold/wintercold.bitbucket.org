> 

在Java中，可以将一个类定义在另一个类里面，或者一个方法里面，这样的类称为内部类，与之对应的，包含内部类的类称为外部类。  

当描述事物时, 事物的内部还有事物,该事物用内部类来描述 -- 内部事物在使用外部事物的内容  

## 一. 内部类特点
> 访问方式, 所处位置

#### 1. 访问方式
内部类可以直接访问外部类中的成员,包括私有成员。(因为内部类中有一个外部类的实例 -- 外部类名.this)  
而外部类要访问内部类中的成员必须要建立内部类的对象。  

#### 2. 位置
**Java中的内部类主要分为四种：成员内部类，静态内部类，方法内部类，匿名内部类。**  

#### 3. 访问顺序
内部类局部变量 -- 内部成员变量 -- 外部类  

#### 4. 修饰符
内部类可修饰为`private`, 在外部类中实现更好的封装. -- 只有成员内部类可以加访问修饰符.  

## 二. 成员内部类
成员内部类是最常见的内部类，又称普通内部类。  
#### 1. 获取内部类对象实例的三种方法

内部类在类外使用时，无法直接实例化，需要调用外部信息才能实例化。   

```java
	// 获取内部类对象实例  方法一：new 外部类 new 内部类
        Person.Heart myheart = new Person().new Heart();
        String str = myheart.beat();
        System.out.println(str);

        // 获取内部类对象实例  方法二：外部类对象.new 内部类
        Person.Heart myheart1 = tom.new Heart();
        String str2 = myheart.beat();
        System.out.println(str2);

        // 获取内部类对象实例  方法三：外部类对象.getHeart()
        Person.Heart myheart3 = tom.getHeart();
        String str3 = myheart.beat();
        System.out.println(str3);
```
#### 2. 内部类 && 外部类

1. 内部类可以直接访问外部类的成员，如果出现同名，优先访问内部类中的属性。  
2. 可以使用**外部类.this.成员**的方式访问外部类中同名信息
3. 外部类不能直接访问内部类信息，要通过内部类实例才能访问  
[成员内部类](https://github.com/EasterFan/JavaExercise/blob/master/innerClass/src/com/easter/member/Person.java)

## 三. 静态内部类
#### 1. 获取内部类对象实例
被static修饰的静态内部类是类共享的，故**静态内部类对象可以不依赖于外部类对象，直接创建**  

```java
	Person.Heart myheart = new Person.Heart();
        myheart.beat();
```
#### 2. 内部类 && 外部类

1. 静态内部类中只能直接访问外部类的静态方法和属性，非静态方法和属性要通过对象实例调用  
2. 外部类通过**外部类.内部类.静态成员**的方式访问内部类中的静态成员。通过**new Person().age**方法访问外部类非静态属性
3. 内部类和外部类静态属性同名时，优先访问内部类属性，也可通过**Person.age**方法访问外部类静态成员  
4. 当内部类中有静态成员时,该内部类必须是静态类.
5. 当外部类静态方法访问内部类时,该内部类必须是静态内部类  
[静态内部类](https://github.com/EasterFan/JavaExercise/blob/master/innerClass/src/com/easter/staticinner/Person.java)

## 四. 方法内部类
#### 1. 获取内部类对象实例
无法直接获取，通过调用该方法产生对象  
方法内部类的意义：  
一是可以隐藏，二是方法内部类对象生命周期仅在方法里有效，方法执行完毕后就会销毁。  
#### 2. 一些限制

1. 方法内部类作用范围是在这个方法内
2. 方法内部类中不能定义静态成员
3. 方法内部类不能用public/private/protected修饰
4. 不推荐使用abstract类修饰方法内部类，这样就不能返回对象方法了
5. 内部类定义在类中的局部位置时,只能访问该局部被final修饰的局部变量.  
[方法内部类](https://github.com/EasterFan/JavaExercise/blob/master/innerClass/src/com/easter/function/Person.java)
### 五. 匿名内部类
#### 意义
一是为了隐藏，二是在整个程序中只使用一次，将类的定义与构建放到一起完成，简化类的编写，内存友好。  

```java
  test2.getRead(new Person() {
            @Override
            public void read() {
                System.out.println("我是读书的小明");
            }
        });
```
#### 注意
1. 匿名内部类无法使用private/public/protected/abstract/static/修饰
2. 因为匿名内部类没有名字，故匿名内部类没有构造方法，但可以通过`构造代码块`完成成员变量的初始化操作
3. 匿名内部类中不能出现静态成员
4. 匿名内部类使用前提:  
内部类必须继承一个类,或者实现接口.(除非使用奇淫技巧)  
5. 匿名内部类就是一个**匿名子类对象**,而且这个子类有点胖
6. 匿名内部类的格式: new 父类或者接口(){定义子类的内容}
7. 匿名内部类中定义的方法最好不要超过3个 -- 简化代码

```java
// 既没有继承也没有接口,使用匿名内部类的奇淫技巧 -- Object
class InnerTest{
    public static void main(String[] args) {

        // 所有对象都是 Object 子类
        new Object(){
            void function(){
                System.out.println("function run");
            }
        }.function();
    }
}

```
[匿名内部类 -- 继承](https://github.com/EasterFan/JavaExercise/blob/master/innerClass/src/com/easter/anonymous/Person.java)  
[匿名内部类 -- 接口](https://github.com/EasterFan/JavaExercise/blob/master/innerClass/src/com/easter/anonymous/InnerClassTest.java)  
