
## 一. 类与对象的概念

```bash
面向过程：放（大象，冰箱）
面向对象：大象（放，冰箱）
```

这里大象就是一个对象，是一个实例。  
面向对象中，类是对象的抽象集合，包是类的管理器，是类的集合。

开发的过程:  
其实就是不断的创建对象,使用对象,指挥对象做事情。  

设计的过程:  
其实就是在管理和维护对象之间的关系  


## 二. 类
> 定义类其实在定义类中的成员(成员变量和成员函数)

### 创建一个类
```java
package com.easter.zoo;

/**
 * 定义一个类：属性+方法
 */

public class Cat {
    // 成员属性
    String name;
    int age;
    double weight;
    String specie;

    // 成员方法
    public void eat(){
        System.out.print("猫吃鱼");
    }

    public void run(){
        System.out.print("猫跑步");
    }

}

```

## 三. 对象
> 创建对象, 对象的内存结构,匿名对象

### 1. 实例化对象
实例化对象要在**main方法**中操作
对象可以调用类的方法，给类的属性赋值

```java
package com.easter.zoo;

public class CatInit {
    public static void main(String args[]){
        //实例化对象
        Cat katty = new Cat();

        // 对象调用类中的方法，并给变量赋值
        katty.eat();
        katty.run();
        katty.age = 18;
    }
}

```

创建类和实例化类写在两个java文件中，遵循设计模式的`单一职能原则`。  

单一原则的好处: 提高代码复用性，方便重构。

### 2. 对象的内存结构
![](../assets/ObjectInMemory.png)  

对象的本质是一个堆内存地址.  
用 = 表示将**堆**中的内存地址传递到**栈**的变量中.  
对象引用就是将堆的地址重新指给栈中变量.  

### 3. 匿名对象
> 匿名对象是对象的简化形式

匿名对象两种使用情况  
• 当对对象方法仅进行一次调用的时(方法执行完后,就被销毁)
• 匿名对象可以作为实际参数进行传递(相比有名对象,传递参数后,引用被销毁,堆内存立即释放)  

```java
// 调用方法
new Cat().eat();

// 传递参数
show(new Cat());

void show(Cat cat){
System.out.print("eat");
}
```

### 4. 对象初始化过程
> `Person jack = new Person("jack",20);`发生的顺序

```java
public class Person {
    private String name;
    private int age;
    private static String country = "china";
    private static String hair;

    static {
        System.out.println("static代码块(演示静态成员变量优先于静态代码块):"+country);
    }
    
    {
        System.out.println("构造方法块:(演示构造方法块优先于构造方法代码块)"+"name"+name+",age"+age+",country"+country + ",hair"+hair);
    }

    Person(String name, int age){
        this.name = name;
        this.age = age;
    }

    public void setName(String name){
        this.name = name;
    }

    public static void main(String[] args) {
        Person jack = new Person("jack",20);
        System.out.println("name"+jack.name+",age"+jack.age+",country"+jack.country);
    }

}
```

1. new用到`Person.class`,故会先找到`Person.class`文件,加载到内存中
2. 执行**静态成员变量**初始化,此时`country=china,hair=null`
3. 执行**静态代码块**,对`Person.class`类进行初始化(优先于分配内存空间 -- 静态代码块优先于对象创建)
4. 在堆内存中开辟空间,分配内存地址 -- 0x0023
5. 执行**构造代码块**,此时name,age还没有被赋值,故显示默认值:`name=null,age  =0`
6. 执行**成员变量初始化**,(将`private String name;`改为`private String name = "Tom";`)此时`name=Tom,age=0`
7. 执行**构造方法**,此时`name=jack,age=20`
8. 将内存地址 0x0023 赋给栈内存中的jack

## 四. 构造方法
> 构造方法是new关键字的好搭档，帮助配置对象初始化

### 1. 构造方法的写法

无参:  
```java
 public Cat(){

    }
```

有参:  

```java
public Cat(String name,int age,double weight,String specie){
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.specie = specie;
    }
```
四个赋值什么作用?  
这四个赋值表示的是，将**参数值**赋给**属性值**  
右边的参数值是构造函数括号里的四个参数，左边的属性值，是Cat类中定义的四个成员变量。  

### 2. this 语句
> 两种用法: 1. 将参数传递给成员变量  2. 构造方法间相互调用
 
1. 将参数传递给成员变量  
this表示当前对象，程序具有就近原则，如果不指定是当前对象的name属性，程序通过就近原则就会找到局部变量name，而不是成员变量name，可以说，我们引入this关键字，是为了解决局部变量和成员变量相同的情况，帮助程序将参数存到堆中的成员变量中。  

当前对象是谁呢？  
哪个对象调用了构造函数，这个对象就是this。  
`Cat Tom = new Cat("Tom",2,8.0,"纯正蓝猫")`，  
Tom在new时调用了上面的有参构造函数，**this**指的就是**Tom**  

2. 构造方法间相互调用
> 有时只暴露一个公共的构造方法,很多私有构造方法相互调用

不允许出现构造函数死循环  
```java
Person(){
this("Jack");
}


Person(String name){
this();
}

```

### 3. 构造方法的特点

1. 构造方法名必须与**类名**相同，且无返回值
2. 构造方法不能由对象单独调用，必须配合new关键字
3. 构造方法可以没有（系统会默认给你一个无参构造方法），可以有多个，他们之间互为重载
4. 构造方法不可以被对象调用，却可以被构造方法调用（通过**this()**）--即构造方法之间相互调用，这句代码必须放在第一行。  
5. 一个类中默认的无参构造函数的权限和所处的类一致, 类有 public 修饰, 构造函数也有 public 修饰. 
