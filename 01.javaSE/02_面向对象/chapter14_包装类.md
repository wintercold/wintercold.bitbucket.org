## 一. 什么是包装类
> 包装类的最常见作用:
基本数据类型 <---> 字符串类型相互转换

Java数据类型分为基本数据类型和引用数据类型，基本数据类型是不具有面向对象特征的，基本数据类型**没有属性和方法**的调用，**不能进行对象交互**，包装类的意义在于让基本数据类型拥有属性和方法，让基本数据类型进行对象化交互。  
![](../assets/javaDataStructure.png)  
**Java中的包装类都被final修饰，不能被继承，没有子类。**  

## 二. 包装类与基本数据类型
包装类和基本数据类型存在对应关系。  
两者通过拆箱装箱相互转化，手动转化和自动转化。  
### 1.对应关系
![](../assets/relationship.png)  

### 2.基本数据类型  ----装箱--->  包装类

```java
	int intNum = 3;
        // 装箱：基本数据类型--->包装类
        // 1. 自动装箱
        Integer selfInBox_integer = intNum;

        // 2. 手动装箱
        Integer handInBox_integer = new Integer(intNum);
```

### 3.包装类  ----拆箱--->  基本数据类型

```java
	Integer integerNum = 4;
	// 拆箱：包装类--->基本数据类型
        // 1. 自动拆箱
        int selfOutBox_int = integerNum;

        // 2. 手动拆箱
        int handOutBox_int = integerNum.intValue();
```
int包装类Integer可以拆箱成double/float/short基本数据类型,因为这四个基本类型的包装类都继承自Number父类。  

## 三. 包装类的作用
> 基本数据类型 --- 字符串类型

### 1.常用方法--基本数据类型（整型）转为字符串

```java
// 基本数据类型转字符串---通过包装类的toString()
        Integer integer = new Integer(intNum);
        String stringFromInteger = integer.toString();
```
### 2.常用方法--字符串转为基本数据类型（整型）
两种方法：parseInt()和valueOf()  

```java
	// 字符串转基本数据类型---1.通过包装类的parseInt()
        int intFromString = Integer.parseInt(string);

        // 字符串转基本数据类型---2.通过包装类的valueOf()
        int intFromString2 = Integer.valueOf(string);

```

[包装类的拆箱装箱Demo](https://github.com/EasterFan/JavaExercise/blob/master/WrapProj/src/BasicAndClass.java)  

## 四. 包装类的注意事项
> 包装类对象间比较   基本数据类型和包装类的异同

### 1.包装类对象间的比较
先上一段Demo

```java
public class Comparation {
    public static void main(String args[]){

        // 1. 等号两边比较的是对象---new关键字创建两个不同的内存空间，内存地址不相等
        Integer one = new Integer(100);
        Integer two = new Integer(100);
        System.out.println("one == two的结果："+(one == two)); // 1

        // 2. 等号两边比较的是int数值---由于自动装箱和拆箱
        Integer three = 100; // 自动装箱
        System.out.println("three == 100 的结果："+(three == 100)); // 2 自动拆箱

        // 3. 等号两边比较的是对象--- three和four指向对象池中同一块内存空间
        Integer four = 100;
//        Integer four = Integer.valueOf(100);
        System.out.println("three == four 的结果："+(three == four)); // 3

        // 4. 等号两边比较的是int数值---由于自动装箱和拆箱
        Integer five = 200;
        System.out.println("five == 200 的结果："+(five == 200)); // 4

        // 5. 等号两边比较的是对象--- 超出对象池范围，new新的对象
        Integer six = 200;
        System.out.println("six == five 的结果："+(five == six)); // 5

 	// 6. 等号两边比较的是对象--- double和float没有对象常量池的概念
        Double seven = 100.0;
	Double eight = 100.0;
//        Double seven = Double.valueOf(100);
        System.out.println("seven == eight 的结果："+(seven == eight)); // 6
    }
}

```
[包装类对象间的比较](https://github.com/EasterFan/JavaExercise/blob/master/WrapProj/src/ObjectPool_Wrap.java)

另外，double和float没有对象常量池的概念。String有对象常量池。number类型的包装类范围为【-128 ～ 127】。
[详见博文](http://easterpark.me/2017/11/11/Java-Object-Pool.html)
### 2.基本数据类型与包装类的异同

- 在 Java 中,一切皆对象,但八大基本类型却不是对象。  

- 存储方式及位置的不同,基本类型是直接存储变量的值保存在栈中能高效的存取,包装类型需要通过引用指向实例,具体的实例保存在堆中。  

- 初始值的不同,包装类型的初始值为 null,基本类型的的初始值视具体的类型而定,比如 int 类型的初始值为 0,boolean 类型为 false。  
