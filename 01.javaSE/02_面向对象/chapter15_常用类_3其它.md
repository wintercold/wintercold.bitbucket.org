> 介绍一些常用对象: System  Runtime  Date  Calendar  Math-Random  
主要在于培养查阅 API 文档的能力

## 一.System
> System 加载的是系统的属性, System 中的属性和方法都是静态的

JVM 启动时, 获取当前系统环境,  
Properties 是 HashTable 的子类, 故可以用 map 的方法遍历 Properties  
[其它类 - System获取当前运行环境]()  

## 二. Runtime

每个java 应用程序都有一个 Runtime 类， 使应用程序能够与其运行的环境连接（java 是跨平台的）  

- 该类没有构造方法，不能直接创建对象，因为java 程序一运行，JVM 自动就创建了对象，不需要程序员自己创建对象，只需要用 getRuntime() 获取这个对象即可

```
该类不提供构造函数 -- 该类不可new 对象，那么直接想到该类中的方法都是静态的（只能用类名调用） -- 如果该类中还有非静态方法 -- 说明该类肯定提供了方法获取本类对象，而且该方法是静态的，-- 并返回值是本类型  
这种方式实现了单例设计模式,保证了对象的唯一性
```
[其他类 - Runtime获取进程杀死进程]()
## 三. Date
> Date 类中很多方法已过时, 使用其相关类 DateFormat 的子类 SimpleDateformat

SimpleDateFormat 创建时传入了一个 pattern 模式, Partten 的作用是把自己定义的模式规则和相关数据联系起来.  

`星期E`走的是本地化 -- 跟着电脑系统决定是`星期三`还是`Wed`  
[其他类 - SimpleDateFormat 自定义时间格式]()  
## 四. Calendar
> Calendar 是一个时间对象,封装了时间元素(键值对方式),用 get()方法获取

**坑点**:  
- 国外的月份比我们少一个月
- 国外每周开始是星期天

故这样写查表法:  
```java
String month[] = {"一","二","三","四","五","六","七","八","九","十","十一","十二"};
String week[] = {"","七","一","二","三","四","五","六"};
```
[其它类 - Calendar查表法单独获得时间元素]()  
[其它类 - Calendar时间元素增量]()
## 五. Math-Random
> 生成 0 - 1 之间的随机数

有两种方法: Math.random 或 Random 类,  
后者使用更简单些,省去了自己强制转换.  

[其他类 - Random随机数]()
