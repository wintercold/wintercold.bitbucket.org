> 泛型: jdk 1.5 的新特性, 用于解决安全问题, 是一个安全机制

集合中可以存储不同类型的对象, 但是我们在遍历的时候,并不知道我们遍历了不同的对象, 从而造成了类型转换异常(Class Cast Exception),  

这个异常是运行时异常, 只有在运行时才产生(根据输入具有随机性,有可能测试时通过,客户运行时出错),  

泛型的功能是, 把这个运行时异常转换为编译时异常, 如果存在类型转换的安全隐患, 程序在编译时就会报错, 防范于未然

泛型的实现是借鉴了数组的特性, 数组在初始化时就定义了数组类型(int 类型或 String类型),如果存入错误的类型,编译都不能通过.  

泛型也是借鉴了这一点, 在集合初始化时,就定义了集合的存储类型,这时,集合就只能存储一种数据类型了`ArrayList<String>`

### 好处
1. 将运行时期出现的问题`ClassCastException`转移到编译时期,方便程序员解决问题,让运行时期问题减少, 安全.  
2. 避免迭代遍历时,强制类型转换的麻烦  
需要将迭代器也设置成泛型, [TreeSet - 让集合具备比较性 - 泛型写法](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/generic/StringTreeSetWithoutClassCast.java)

## 一. 泛型类
迭代器选择器通过实现接口,来实现泛型, 自定义的类如何具有泛型属性呢? -- 继承泛型类  

为什么要让自定义的类定义泛型?  
对于工具类, 要接收多个不同类型的对象, 没有泛型机制时, 通过使用 Object 对象向上转型来接收所有对象, 但这样有一个问题: 在读取 Object 时, 可能会出现类型转换异常, 泛型可以让类型转换异常在编译时暴露出来.  

什么时候定义泛型类?  
当类中要操作的对象有多个不同对象的时候(或对象类型不确定的时候),  
早期用 Object 完成扩展,  
现在用泛型完成扩展.  

```java
/**
 * 现在的泛型写法  -- 泛型类
 */
class GenericTool<Animal>{
    private Animal animal;

    public void setAnimal(Animal animal){
        this.animal = animal;
    }

    public Animal getAnimal(){
        return animal;
    }
}
```
[泛型类 -- 工具类拓展](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/generic/GenericClass.java)

## 二. 泛型方法
> 泛型不仅可以定义在类上, 还可以定义在方法上,泛型方法比泛型类更灵活

### 1. 泛型方法使用场景
什么时候使用泛型类, 什么时候使用泛型方法?  

若一个类**接收的对象类型不确定**时, 使用泛型类,  
若一个方法**接收的对象类型不确定**时, 使用泛型方法

### 2. 泛型写法
泛型方法的格式: 写在修饰符之后, 方法返回值之前  

```java
public <F> void show(F f){
        System.out.println("show:"+ f);
    }

    public <F> void print(F f){
        System.out.println("print: " + f);
    }
```
[泛型 -- 泛型方法](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/generic/GenericFunc.java)

### 3. 静态方法的泛型
静态方法不能使用定义在类上的泛型 -- (静态方法优先于类加载进内存, 静态方法执行时, 泛型类还没有加载进内存, 故无法使用)  

若静态方法接收的对象数据类型不确定, 只能在静态方法上加泛型
```java
  // 静态方法泛型
    public static <P> void method(P p){
        System.out.println(p);
    }
```

## 三. 泛型接口
> 不多见, 有两种实现方式

```java
interface  inter<T>{
    public void show(T t);
}

/**
 * 实现一: 在实现接口时,就指定泛型类型
 */
class Interimpl implements inter<String>{

    @Override
    public void show(String string) {
        System.out.println("实现一: 在实现接口时,就指定泛型类型" + string);
    }
}

/**
 * 实现二: 在实现接口时, 仍然不指定泛型类型
 * @param <T>
 */
class InteImplment<T> implements inter<T>{

    @Override
    public void show(T t) {
        System.out.println("实现二: 在实现接口时, 仍然不指定泛型类型" + t);
    }
}
```
[泛型 -- 泛型接口](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/generic/GenericImpl.java)

## 四. 泛型高级应用 -- 泛型限定
> 泛型限定的范围:  
1. 通配符: ?
2. 上限: ? extends Person
3. 下限: ? super Person

### 1. 泛型限定 -- 通配符
使用泛型通配符后, 就不能调用具体的属性,因为传入的对象类型未知,无法确定调用的是哪个对象的哪个属性
```java
public static void printColl(ArrayList<?> arr){
        for(Iterator<?> it = arr.iterator();it.hasNext();){
            System.out.println(it.next());
            // 不可调用原来的属性
            System.out.println(it.next().length);
        }
    }
```
就和多态一样, 优点是提高了扩展性, 缺点是不能再调用原来的方法了
[泛型限定 -- 通配符](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/generic/GenericFanwei.java)

### 2. 泛型限定 -- 上限
> 上限: ? extends Person ==> 接收 Person 类型和 Person 的子类型

```java
// 上限, 接收 Person22 和 Person22 的所有zi类 -- 可调用特有属性
public static void printColl(ArrayList< ? extends Person11> per){
        for(Iterator<? extends Person11> it = per.iterator();it.hasNext();){
            System.out.println(it.next().getName());
        }
    }
```
TreeSet 的构造器就用到了上限限定
[泛型限定 -- 上限](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/generic/GenericUpper.java)

### 3. 泛型限定 -- 下限
> 下限: ? super Person ==> 接收 Person 类型和 Person 的父类型

```java
 // 下限, 接收 Student22 和 Student22 的所有父类 -- 不可调用特有属性
    public static void printSuper(TreeSet<? super Student22> tree){
        for(Iterator<? super Student22> it = tree.iterator();it.hasNext();){
            System.out.println(it.next().toString());
        }
    }
```
[泛型限定 -- 下限](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/generic/GenericLower.java)
