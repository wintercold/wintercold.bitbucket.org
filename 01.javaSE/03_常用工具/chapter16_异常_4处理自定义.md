## 一. 自定义异常
> 因为项目中会出现特有的问题(该类问题并没有被 java 描述并封装成对象)，可以通过**自定义异常**描述特定业务产生的异常类型  

### 1. 自定义异常的特点
1. 所谓自定义异常,就是定义一个类,去**继承Throwable类**或者它的子类(Exception)。  
推荐继承 Exception 类, 因为 Exception 是 Throwable 的子类, 拥有更丰富的方法, 如果想要新建一个继承体系,则继承 Throwable.  
2. 自定义的异常类, java 并不认识, 需要手动**建立对象**并抛出(throw).  
3. 当函数内部已经用**throw**抛出异常对象,那就必须要给出对应的处理动作(两种处理方式)  
  - 在函数名处 throws 声明异常
  - 自己 try  catch 处理
4. 父类 Throwable 中已经把异常信息的操作都完成了,子类只需要在构造函数中, 使用`super(String msg)`传入异常信息即可.然后用`getMessage()`获得该参数.  
5. 为什么自定义类要继承异常类?  
因为(Throwable)异常体系具备一个特点:  **异常类**和**异常对象**都可以被抛出, 使程序发生跳转 ,继承后,程序就有了跳转功能.  
且只有这个体系中的类和对象才可以被 throw 和 throws 操作.  

### 2. 自定义异常类的写法
```java
// 一个自定义异常类的写法
class FuShuException extends Exception{
    private int value;
   // 默认构造函数
    FuShuException(){
        super();
    }

    // 接收子类异常信息
    FuShuException(String msg,int value){
        super(msg);
        this.value = value;
    }
    // 获取输入的负数
    public int getValue() {
        return value;
    }
}
```
[自定义异常类 -- 被除数不为负数](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_05_CustomThrowFushu.java)   
[自定义异常类--网吧年龄限制](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_05_CustomThrow.java)  

## 二. RuntimeException 运行时异常
> 运行时异常,也许是因为这个异常,是用户错误输入(空指针, 数组下标越界, 算术异常, 类型转换)造成的,调用者不方便处理,直接挂掉,让用户重新输入

### 1. 运行时异常特点
1. 在函数内手动抛出运行时异常时,不需要在函数名处用`throws`声明.编译通过  
2. 如果在函数上声明了该异常, 调用者可以不用处理,编译一样通过.  
3. 之所以这样设计, 不在函数名处声明, 是为了不让调用者处理异常, 程序运行时,直接挂掉,然后让程序员修正代码
4. 自定义异常时, 如果该异常的发生, 是用户错误输入造成的,调用者也不方便处理, 就让自定义异常继承 **RuntmeException**
5. 普通异常发生后, 通过 throws 或 trycatch 处理异常,可以让程序继续运行  
RuntimeExcetion 的异常发生后, 不要声明 throws, 也不要用 trycatch 处理, 目的就是为了让程序,在这个异常发生后, 马上停止.  
如果调用者用 trycatch 处理了 RuntimeException 异常, 程序就可以跳过这个异常,继续往下运行.  
但是,我们使用RuntimeException的目的就在于:  让程序就停在这里, 不要往下运行了(因为这个关键异常已经出现, 再往下已经没有计算的必要了)

[RuntimeException - 直接让程序挂掉的使用](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_10_RunTimeExceptionTest.java)  

## 三. 异常链

### 1. 异常与继承关系
> 异常在子父类覆盖中的体现

###### 1.1 父类一个异常
> 1. 子类方法抛出的异常范围 <= 父类方法抛出的异常范围  
> 2. 继承关系中, 子类重写父类方法,抛出的异常,只能是**父类异常**或者**父类异常的子异常**.  
> 3. 即: 子类重写父类的方法, 子类的异常只能是父类中已经定义的异常, 或者父类的子异常, 子类不能抛出新异常.  


如果子类非要抛出一个新异常, 只能在子类内部 trycatch, 自行解决异常, 不能用 `throws`抛出  

[异常与继承--子类异常<=父类异常](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_09_ExtendsInExceptionTest.java)  

###### 1.2 父类多异常
> 如果父类方法抛出多个异常, 那么子类在覆盖方法时,只能抛出父类异常的子集.  

若父类throws了5个异常: A B C D E   
则子类只能 throws 5 个以下的异常

###### 1.3 父类多异常
>  若父类方法无异常抛出, 则子类重写的方法,不能抛出任何异常,若子类必须要抛出异常,只能在子类方法内部自行 trycatch 解决.绝对不能throws抛出.  

### 2. 异常链 - 获取前面的异常
在异常链中，最后的异常会覆盖前面的异常，怎样获取前面被覆盖的异常呢？  

[异常链](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_06_ThroableLine.java)  
先在前几个异常中封装e  
```java
throw new Exception("我是test2中抛出的异常",e);
```

最后抛出的时候新建一个异常对象e1，初始化前几个异常
```java
// 封装前两个异常
            Exception e1 = new Exception("我是test3中抛出的异常");
            e1.initCause(e);
            throw e1;
```
[异常链 - 异常中的子类](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_07_ThrowsInherianceSon.java)  

[异常案例 -- 老师讲课异常!](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_08_TeacherExceptionTest.java)  
