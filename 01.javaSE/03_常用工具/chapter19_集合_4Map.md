> map 集合(双列集合) 和 Collection 集合(单列集合)并列

Map 集合特点 : 该集合存储键值对, 键具有唯一性, 值可以重复  

Map 三个实现类: HashTable  HashMap  TreeMap  

## 一. Map 集合的共性方法

### 1. Map 共性方法
- 添加
  - put(K key, V value)  -- 返回原来的值(键相同时(通过重写hashcode和equals判断键是否相等), 新值覆盖旧值)
  - putAll(Map<? extends K,? extends V> m)

- 删除
  - clear()
  - remove(Object key)

- 判断
  - containsKey(Object key)
  - containsValue(Object value)
  - isEmpty()

- 获取
  - get(Object key)
  - size()
  - values()
  - **entrySet()**
  - **keySet()**


### 2. Map 特性方法
> Map 没有迭代器(其键值对存储方式,不需要迭代), 有两种方式取出所有元素: map --> Set --> Iterator

方法一:  keySet()   
```java
Set<String> key = map.keySet();
        for(Iterator<String> it = key.iterator();it.hasNext();){
            String key1 = it.next();
            String value = map.get(key1);
            System.out.println(key1 +"...."+ value);
        }
```

方法二: entrySet() -- 将Map中的**映射关系**取出, 存入到 Set 集合中
```java
   Set<Map.Entry<String,String>> mapentry = map.entrySet();

        for(Iterator<Map.Entry<String,String>> it = mapentry.iterator();it.hasNext();){
            Map.Entry<String,String> me = it.next();

            String entrykey = me.getKey();
            String entryvalue = me.getValue();
            System.out.println("key: "+ entrykey + ",value: " + entryvalue);
        }
```
`map.entrySet`返回的是一个 Set 集合, 集合里面盛放的是 map 集合的映射关系, 每一个映射关系的数据类型是:`Map.Entry`类型.  

`Entry`是map的内部接口,所以用`Map.Entry`调用  
[两种方式遍历map](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/map/MapFunc.java)

## 二. HashTable
> 底层实现 -- 哈希表 , 不允许 null 作为键或值, 该集合线程同步, jdk1.0, 效率低

## 三.HashMap
> 底层实现 -- 哈希表 , 允许 null值 和 null键, 该集合线程不同步, jdk1.2, 效率高

同 HashSet 一样, HashMap的存储是无序的, 通过重写 `Hashcode + equals`判断集合元素的唯一性.  
[HashMap - 对学生的无序存取](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/hashmap/MapTest.java)

## 四. TreeMap
> 底层实现 -- 二叉树, 不同步, 可以对 map 集合中的键排序

同TreeSet一样, TreeMap 有两种方式,实现对键的排序:  
- 让对象实现`Comparable`接口, 重写`CompareTo`方法, 使对象具有比较性
- 让集合实现`Comparator`接口,重写`compare`方法, 使集合具有比较性


[TreeMap - 对学生的有序存取](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/treemap/TreeMapTest.java)  
[TreeMap练习 -- 数字母数](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/treemap/TreeMapCountNum.java)

## 五. Map 集合的扩展
> map 存储 map

Map 集合存储的是一对一的映射关系, 但是, Map 也可以存储一对多的关系 --- 在 Map 中存入集合.  
Map 集合的扩展使用, 体现在数据库一对多的表结构中

一个学校, 对应多个班级, 一个班级,对应多个学生, 每个学生学号对应一个姓名
[Map 扩展 - Map 存 Map](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/map/MapInMap.java)  
一个学校, 对应多个班级, 一个班级,对应多个学生对象, 每个学生对象里有学号和姓名属性    
[Map 扩展 - Map 存 set](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/map/ListInMap.java)
