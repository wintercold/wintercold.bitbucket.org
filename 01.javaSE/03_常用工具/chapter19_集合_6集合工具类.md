> 两个集合工具类: Collections 和 Arrays, 其中都是静态方法

工具类的特点:  
没有对外提供构造方法, 不需要创建对象, 因为它的对象中没有封装特有数据,都是共享型情况下,定义成静态最方便

## 一. Collections

### 1. 对 List 集合排序
List集合本身不具备比较性, 想对List集合进行排序 -- Collections 的sort方法  

`public static <T extends Comparable<? super T>> void sort(List<T> list)`  
- T extends Comparable:  
该泛型限定对象必须实现`Comparable`接口 - 即该对象是 `Comparable`的子类, 使对象具备比较性.  

- Comparable<? super T> :  
Comparable 本身就有一个泛型限定, 限定接收该对象和该对象的父类(比较的时候有很多子类进行比较)  

[Collections 工具类--sort两种用法](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/utilCollections/SortCollection.java)

### 2. List 集合二分查找 -- binarySearch
> 只有 list 集合才可以用二分查找 -- 二分查找必须要有角标

- 使用二分查找前**必须**对list集合进行 sort 排序,
- 如果二分查找需要传入比较器, 那么 sort 时也需要传入相同的比较器

[Colections工具类 -- 二分查找传入比较器](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/utilCollections/BinarySearchCollection.java)  

### 3. 替换反转
> replace , reverse

1. reverseOrder -- 强行逆转比较器  
reverserOrder返回一个比较器, 该比较器可以把原比较器反转(自然排序 --> 倒序  从短到长 --> 从长到短)  

[Colections工具类 -- ReverseOrder 比较器反转](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/utilCollections/ReverseOrder.java)

### 4. 集合同步
> 集合中所有对象都有一个共同的特点 -- 线程不安全(因为高效), 如果集合对象被多线程操作时,怎样安全使用(自己加锁很麻烦)? --- synchronizedList/Map/Set

[Colections工具类 -- Sync同步集合](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/utilCollections/SyncCollections.java)

## 二. Arrays

### 1. equals 和 deepEquals
equals 比较两个数组类型是否相等(数组元素是否为同一数据类型--int,boolean),  
deepEquals 比较两个数组类型是否相等 && 数组元素是否相同  

### 2. toString 数组转字符串
> 以前用 StringBuffer 做的, 现在可以直接用 Arrays.toString方法

[Arrays工具类 -- toString数组 --> 字符串](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/utilArrays/ArrayToString.java)  

### 3. asList 数组转 List集合
为什么要把数组转为list集合?    
好处: 可以用集合的思想和方法来操作数组, 否则要自己写各种方法  
数组的方法较少,集合的方法很多.  

**注意:**  
1. 数组转为集合后, 不能用集合的**增删方法**,  
因为数组长度是固定的, 集合长度是可变的  
2. 数组转集合的两种情况  
如果数组中的元素都是对象, 那么变成集合时,数组中的元素直接转为集合中的元素,  
如果数组中的元素都是基本数据类型, 那么变成集合时,该数组转为集合中的一个元素,   
因为集合中只能存放对象, 数组中既可以存放对象,又可以存放基本数据类型

[Arrays工具类 -- 数组 --> 集合](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/utilArrays/ArrayToList.java)  

### 4. 集合转数组
为什么要将集合变数组?  
为了限定对集合的操作,自己对集合操作完成后, 不希望别人对该集合进行增删操作(只允许查询), 故将该集合转为数组返回  

[Arrays工具类 -- 数组 <-- 集合](https://github.com/EasterFan/JavaExercise/blob/master/MapProj/src/utilArrays/CollectionToArray.java)
