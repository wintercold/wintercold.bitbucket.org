# Set集
>set是元素无序并且不可以重复的集合, Set 的底层实现是 Map

## 一. Set 的特性
Set 接口的方法和 Collection 完全一致, 没有特有方法

### 1. 比较两个对象是否相同的方法
HashSet 判断两个元素是否相同, 以及删除元素, 依靠的都是 `hashcode + equals`
## 二. Set 的实现类 -- HashSet
> HashSet 底层数据结构是 哈希表, 线程非同步

1. HashSet中只允许一个null元素  
2. 具有良好的存取和查找性能  
4. HashSet的底层是哈希表,比较两个对象是否相同采用的是**hashCode + equals**  
5. hashset使用泛型消除安全隐患

### 1. HashSet 怎样保证元素的唯一性呢?  
> 通过 hashCode + equals

Set 集合的无序指的是存入的顺序和取出的顺序是不同的.

你存入对象的时候, HashSet 存入的是内存地址的哈希值, 并按照哈希值的大小排序.  

如果两个对象的哈希值相同(HashCode被重写的情况下), Set 集合会再比较一下两个对象的类型是否相同, 如果对象类型和哈希值都相同, 则存储失败.   

hashCode和equals方法用来提高查询速度，hashCode用来判断在哪个桶里，equals用来判断是桶里的哪个元素。  

### 2. 在HashSet中存放自定义类
set存放自定义类时, 编译器无法比较自定义类是否相同======>在该类中重写hashCode和equals方法。

[hashSet 在自定义类中重写hashcode和equals](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/hashset/Pet_set.java)

## 三. Set 的实现类 -- TreeSet
> TreeSet 底层实现是二叉树 -- 因为TreeSet每存入一个元素, 都要和集合中已有元素比较, 故使用二叉树快速比较, 节约时间

**TreeSet 进行排序的依据** 有两个:  
- 让对象实现`Comparable接口`, 使对象具有可比性
- 当对象自身不具备比较性, 或具备的比较性不是所需要的, 这时就需要让集合自身具备比较性

这两种比较方式,就好像体测身高的教室,   
第一种, 让对象具备比较性, 每个学生进入后, 自己和前面的同学比身高, 对象自己调整, 按身高排序,即学生对象自己拥有排序方法;  
第二种, 让集合具有比较性, 学生进入教室后, 教室拥有一个身高测量表, 自动采集学生身高数据, 进行排序.

### 1. 让对象具备比较性
> 自定义类实现 Comparable 接口, 并重写`CompareTo`方法, CompareTo 方法中定义二叉树存储规则, 1表示大数存右边, -1 表示小数存左边, 0 表示重复元素, 不存入

可以对集合中的元素进行排序, 往TreeSet中存储的对象必须具有**比较性**,使得 TreeSet 了解对象的排序规则    
怎样让对象具有比较性? -- 让对象实现 Comparable接口, 并重写 `CompareTo`方法即可  

CompareTo 返回值有三个(-1 左数, 1 右数), 0 表示重复对象, 插入失败.  

```java
// 学生对象的排序规则 -- 按照年龄排序
    @Override
    public int compareTo(Object obj) {
        if(!(obj instanceof Student))
            throw new RuntimeException("传入的不是学生类");
        Student stu = (Student)obj;

        // 判断主要条件和次要条件
        if(this.age > stu.age)
            return 1;
        // 当年龄相同的情况下(如果返回值为0,则记录存不进去), 应该再判断姓名, 按照姓名排序
        if(this.age == stu.age)
            return this.name.compareTo(stu.name);
        return -1;
    }
}
```
[让对象具备比较性](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/treeset/treeSetPaiXu.java)  

TreeSet 保证数据唯一性的依据:  
**CompareTo return 0**  
TreeSet 在使用contains 包含和删除时,都会调用底层的CompareTo, 判断元素是否存在

那么怎样实现, TreeSet 怎样存入,就怎样取出呢(不按照大小顺序排序)?  
- 只需要在重写`CompareTo方法`时, 直接`return 1` 或 `return -1`即可

### 2. 让集合具有比较性
> 在创建 TreeSet 集合时,传入比较器, 使容器具备比较性

当对象比较性和容器比较性同时存在时,  
容器比较性会覆盖对象比较性;  

开发中容器比较性更为常用.

容器比较性的实现 -- 实现Comparator 接口, 重写 compare 方法  
```java
class MyComparator implements Comparator{

    @Override
    public int compare(Object o, Object t1) {
        Student2 stu1 = (Student2) o;
        Student2 stu2 = (Student2) t1;

        // 按照姓名排序
        int num = stu1.getName().compareTo(stu2.getName());
        // 如果姓名相同, 按照年龄排序
        if(num == 0){
            if(stu1.getAge() > stu2.getAge())
                return 1;
            if(stu1.getName() == stu2.getName())
                return 0;
        }
        return num;
    }
}
```
[让集合具备比较性](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/treeset/TreeSetOrderByCollection.java)  
[练习: 根据字符串长度进行排序](https://github.com/EasterFan/JavaExercise/blob/master/SetProj/src/treeset/StringTreeSetDemo.java)
