> 线程管理 -- 同步和通信

## 一. 同步
> 同步易造成死锁问题

### 1. 什么是同步
一个共享对象，多个线程共享该对象，该共享对象在任意时刻只被一个线程单独使用，这种情况下，多个线程之间的关系，就叫做同步。

### 2. 引入线程同步的原因：  
各个线程是通过竞争CPU时间而获得运行机会的，各线程什么时候得到CPU时间,占用多久,是不可预测的，  
所以经常出现一个正在运行着的线程执行到一半就被迫暂停，另一个线程抢占到马上执行，这样两次操作数据，导致共享数据不对称。  
引入互斥后，一个线程在操作这个对象时，不允许其他线程对这个对象进行操作。  
同步的弊端: 死锁(同步中嵌套同步)  
### 3. 解决方法 -- 同步代码块
1. 明确哪些代码是多线程运行代码
2. 明确共享数据
3. 明确多线程运行代码中,哪些语句是操作共享数据的 -- 用同步代码块扩起来.

### 4. 解决方法 -- 双重校验锁
> 懒汉式

[双重校验锁解决懒汉式线程安全](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/Lock2.java)  

### 5. 同步的弊端: 死锁
> 同步中嵌套同步

```java
  @Override
    public void run() {
        if(flag){
            // A 中 锁 B
            synchronized (Lock.locka){
                System.out.println("if locka");

                synchronized (Lock.lockb){
                    System.out.println("if lockb");
                }
            }
        }else {
            // B 中锁 A
            synchronized (Lock.lockb){
                System.out.println("else lockb");

                synchronized (Lock.locka){
                    System.out.println("else locka");
                }
            }
        }
    }
```
[死锁示例 - obj 套 this 锁](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/DeadLock2.java)  
[死锁示例 - 同步代码块相互嵌套](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/DeadLock.java)

### 6. 同步的意义
好处: 解决了多线程的安全问题  
弊端: 每个线程都要判断锁, 相对消耗资源
## 二. 通信
> 通信 - 等待唤醒机制

### 1. 一个经验
> 多生产者和多消费者的线程通信会导致数据紊乱

究其原因，是因为线程醒来后是否进行再判断（if不判断，while判断）-- 故使用 while 再判断，但是 while 会引起死锁 -- 采用 notifyAll()唤醒所有程序  

当只有一个生产者，一个消费者时，可以用`if + notify`方式  

[线程通信--一个生产者和消费者](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/ThreadCommunicate1/ThreadOneCustom.java)  
[线程通信--多个生产者和消费者](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/ThreadCommunicate2/ThreadManyCustom.java)  

当有多个生产者多个消费者时，使用通用方式 `while + notifyAll`解决线程安全问题  

### 2. 解读
```java
 // 录入人员信息 - 存在线程安全(只录入姓名，性别未输入，被打断)
    public synchronized void set(String name,String sex){
        // 如果 Room 中已有人， 停止录入
        while (flag) {
            try {this.wait();} catch (InterruptedException e) {e.printStackTrace();} // 很有可能线程在这一步阻塞了
        }
        this.name = name;
        this.sex = sex;
        System.out.println("PeopleIn:"+name + "......." + sex);
        flag = true;
        // 唤醒线程
        this.notifyAll();
    }
```
- if 和 while 的区别  
假设线程在`wait()`处阻塞，当线程重新醒来后，  
if 语句是不会再判断`if(flag)`了，直接往下执行 -- 导致生产两次；  
while 语句还会重新判断`while(flag)`，如果满足条件，继续休眠  

- 为什么使用`notifyAll`  
while 比 if 更安全，但 while 语句会导致死锁  
`notify()`只能唤醒线程池中先休眠的线程  
`notifyAll()`可以唤醒所有线程，解决死锁  

**故：一般都采取 `while + notifyAll()` 形式**  

### 3. 一个补充
> jdk1.5 后的新特性 -- java.util.concurrent.locks Lock 代替 synchronized 锁

notifyAll()虽然解决了死锁问题，但是notifyAll()唤醒的是所有线程，在多生产者和多消费者中，作为生产者，只需要唤醒消费者线程即可，不需要再唤醒本方的消费者，怎样实现有选择的唤醒对方线程呢？  

使用：  
synchronized 被 Lock 替代，  
wait，notify,notifyAll 被 Condition 替代   

从前 synchronized 一个锁里只能绑一个对象，一个锁只能对应一个wait(),notify()，如果想要使用多个wait，notfy方法，就要创建多个sychronized锁,创建多个锁后，又易于死锁；    
现在 lock 将wait，notify等方法封装成了 condition 对象，使一个锁里可以绑定多个对象，  
从而实现，唤醒对方线程  

```java
 // 新建一个锁
    private Lock lock = new ReentrantLock();

    // wait，notify等方法都封装在 Condition 对象中，要通过 Lock 获取 Condition 对象
    // 新建两个锁
    private Condition condition_producer = lock.newCondition();
    private Condition condition_customer = lock.newCondition();
```
[线程通信-- lock + Collecton 代替 synchroniised + Object 的写法](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/ThreadCommunicate3/ThreadWithLockWithoutSynchronized.java)  
[线程通信-- lock + Collecton 代替 synchroniised + Object 的写法（实现唤醒对方线程)](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/ThreadCommunicate4/ThreadNotifyOther.java)

## 三. 总结

线程同步：让线程**单独**使用CPU。  
线程通信：让线程**有序**使用CPU。  

多线程的发展历程：  
synchronized 同步（易死锁） --> 一个生产者，一个消费者 --> 多个生产者，多个消费者 --> Lock 锁机制替代 synchronized + while + notifyAll
