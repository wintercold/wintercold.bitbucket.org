# Java集合
> 数据多了用对象存, 对象多了用数组和集合存

## 一. 集合是什么

面向对象语言对事物的体现都是以对象的形式,所以为了方便对多个对象的操作,就对对象进行存储,集合就是存储对象最常用的一
种方式。
1. 数组和集合的区别?
  - 两者都可以存储对象, 数组长度固定，集合长度可变  
  - 数组中可以存储基本数据类型,集合只能存储对象
  - 集合可以存储不同类型的对象, 数组只能存储同一种数据类型
(jdk1.5以后, 基本数据类型有自动拆箱装箱动作,所以集合中存储的基本数据类型,都被自动转成了对象)  

2. 集合的使用场景  
```bash
无法预测存储数据的数量
同时存储具有一对一关系的数据(键值对)
需要进行数据的增删改查
数据重复问题
```

3. 集合的体系
> 为什么会出现这么多容器?因为每一个容器对数据的存储方式不同, 这个存储方式 -- 数据结构

![](../assets/collection_structure.png)  

list: 序列 Queue: 队列 set: 集  
list和Queue存放有序可重复的值，元素存取是有序的  
set存放无序不可重复的值，元素存取是无序的  
map存放不可重复的键值对 。  

## 二. 集合体系的共性方法
> Collection 父接口中的增删查改方法

### 1. 父接口中的增删改
|增   |删           |查   |改                                |判断              |
|----|------------|----|---------------------------------|----------------|
|add |remove clear|迭代器 |retainAll-->保留交集  removeAll-->删除交集|contains  isEmpty|

### 2. 迭代器取出对象  -- 查
> 娃娃机中的夹子就是一个迭代器, 对集合的遍历有两种: 迭代器 || 增强型for循环

因为 list , set , map 底层对数据的存储方式不同, 无法在外部定义一个通用的方法来取出三个容器中的对象,  

所以就把取出方法定义在三个集合的内部, 这样, 这个定义在集合内部的取出函数就可以直接访问集合内部存储的所有对象.  

于是,这个取出函数就被定义成了内部类  

每一个容器的数据结构不同, 所以取出的动作细节也不一样, 但是都有共性内容: 判断和取出 -- 所以可以将共性抽取成一个Iterator接口(统一的规则),  

然后通过一个对外提供的方法`iterator()`让调用者获取集合的取出对象.  

```java
// 迭代器遍历
        Iterator it = list.iterator();
        while(it.hasNext()){
            System.out.println("iterator遍历结果:"+it.next());
        }

// 迭代器在局部变量中, 这种写法, 使迭代器用完后就释放, 内存友好        
        for(Iterator iterator=list.iterator();iterator.hasNext();){
            System.out.println(iterator.next());
        }

// 增强型 for 循环遍历集合
        for(String i : list){
           System.out.println(i);
        }
```
[Collection接口共性方法](https://github.com/EasterFan/JavaExercise/blob/master/ListProj/src/list/CollectionCommonFunction.java)

**增强型for 循环和 迭代器的区别:**  
两者都可以遍历集合, 但增强型for循环更有局限性:  
增强型for循环只能查找集合, 不能进行增删操作;  
Iterator 可以进行查,删操作;  
ListIterator 可以进行增删查改操作.

**传统for循环和高级for循环的区别:**  
高级for循环具有局限性 -- 必须要有被遍历的对象,即不能这样写(`for( : )`)  
遍历集合时,建议使用传统for, 因为传统for可以操作角标,

**高级for循环遍历 Map**  
```java
 HashMap<Integer,String> map = new HashMap<>();

        map.put(1,"aa");
        map.put(2,"bb");

        Set<Integer> keySet = map.keySet();
        for(Integer i : keySet){
            System.out.println(i);
        }

        Set<Map.Entry<Integer,String>> entrySet = map.entrySet();
        for(Map.Entry<Integer,String> entry : entrySet){
            System.out.println("key : " + entry.getKey() + ",value :"+ entry.getValue());
        }
```

### 3. 三大集合间的区别
> 1. List 元素是有序的, 元素可以重复, 因为该集合体系有索引
2. Set 元素是无序的, 元素不可以重复
