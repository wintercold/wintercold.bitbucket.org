# List接口
> List 接口有三个实现类: ArrayList  LinkedList   Vector  
涉及频繁的增删, 少量的查找 -- LinkedList  
涉及频繁的查找, --- Arraylist(一般情况下都用Arraylist)

## 一. List 接口特有方法
> List 的特点, 就是在增删查改时传入了序号参数, 精准的进行插入,删除操作

### 1. List 接口的增删改方法

|增                                           |删             |查                                          |改                 |
|--------------------------------------------|--------------|-------------------------------------------|------------------|
|add(index,element)  addAll(index,collection)|remove(index)|get(index)  subList(from,to) listIterator()|set(index,element)|

[List接口特有方法](https://github.com/EasterFan/JavaExercise/blob/master/ListProj/src/list/ListDemo.java)
### 2. List特有方法 -- ListIterator
> ListIterator 是 List 接口中特有的迭代器, ListIterator 是 Iterator 的子接口

**为什么用ListIterator?**   

有一种需求, 要求用 Iterator 迭代 List 容器, 一边迭代一边操作 ArrayList 列表,  

如果用`list.add`来操作, 会抛异常, 因为 arraylist 对象和 Iterator 对象同时在操作列表中存储的对象, 这样并发访问会造成数据不一致的安全隐患,  

如果想要一边迭代一边操作, 只能用迭代器的方法, 但是Iterator迭代器方法有限(只有三个), 其子接口`ListIterator`有很多方法  

```java
  ListIterator li = list.listIterator();
        while (li.hasNext()){
            Object obj = li.next();
            if(obj.equals("Sept")){
                // 增删改查都可以
                li.remove();  // 删除当前迭代对象
            }
        }
```
[List接口特有迭代器 -- ListIterator](https://github.com/EasterFan/JavaExercise/blob/master/ListProj/src/list/ListIteratorTest.java)

### 3. List 元素的比较
List 元素判断元素是否相同, 判断的是元素的 **equals** 方法  
所以要重写 equals 方法, 使 `contains remove` 在进行比较时, 自动调用重写的 `equals`

## 二. List实现类 -- ArrayList
> 底层使用的是数组结构 - 查询速度快, 增删慢

1. ArrayList 是一个可变长度数组, 是通过50%重新 new 数组实现的
2. 在列表尾部插入或删除非常有效，更适合查找和更新元素  
3. ArrayList底层是由数组实现的，所以在ArrayList中部插入非常耗时，他适合在尾部操作，不适合在中部操作  
4. ArrayList中的元素可以为null  

[ArrayList去重](https://github.com/EasterFan/JavaExercise/blob/master/ListProj/src/arraylist/ArrayListSingle.java)  

Arraylist 存储自定义类并比较对象是否相同时,不能简单的使用contains, 需要重写equals方法  

```java
/**
     * 比较两个对象是否相同 -- 只比较 id 和 title
     * 重写equals
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Notice)) return false;
        Notice notice = (Notice)obj;

        return this.id == notice.id && this.title.equals(notice.title);
    }
}
```
[ArrayList中添加自定义类的Demo](https://github.com/EasterFan/JavaExercise/blob/master/ListProj/src/arraylist/NoticeTest.java)  

## 三. List 实现类 -- LinkedList
> 底层实现的是链表结构 - 查询速度慢, 增删快

### 1. LinkedList 的特有方法
- 删除  
  - popFirst / popLast(如果列表为空,返回null, 老版本remove直接抛异常)
- 获取
  - peekFirst / peekLast(如果列表为空,返回null, 老版本get直接抛异常)
- 添加
  - offerFirst / offerLast

[LinkedList 模拟堆栈或队列](https://github.com/EasterFan/JavaExercise/blob/master/ListProj/src/linkedlist/LinkedListTest.java)

## 四. List 实现类 -- Vector
> 底层使用的是数组结构

Vector 是同步的（安全， 速度慢），ArrayList 是异步的, 现在 Vector 已经被ArrayList 完全替代了, 多线程中自己加锁, 也不用 Vector

Vector 是在集合框架出现之前就存在的, 比较老, 当时还没有Iterator迭代器, 所以取出 Vector 中的存储对象, 用 `Enumeration` 枚举取出,   

`Enumeration` 和 `Iterator`是一样的, 前者是 Vector 的特有取出方式(因为自集合框架后, 所有集合都统一了 Iterator 迭代取出方式).  

因为`Enumeration` 的方法名都过长, 使得它已经被`Iterator`完全取代了.  

```java
// 枚举取出
        Enumeration en = v.elements();
        while(en.hasMoreElements()){
            System.out.println(en.nextElement());
        }
```

[Vector 枚举取出对象元素](https://github.com/EasterFan/JavaExercise/blob/master/ListProj/src/vector/VectorDemo.java)
