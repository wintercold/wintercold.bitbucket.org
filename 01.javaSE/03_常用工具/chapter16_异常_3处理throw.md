> thorws用在函数上,后面跟**异常类名** -- 用于告诉调用者,该函数存在异常  
throw用在函数内,后面跟异常对象。 -- 用于抛出异常对象;

## 一. Throws 声明异常类
#### 1. throws 特点
- 如果一个方法中可能出现可查异常,要么用try-catch语句捕获,要么用throws声明**异常类型**将它抛出（谁调用了这个方法，谁就会接收这个异常并处理）,否则会导致编译错误  

- 使用场景：如果一个方法可能会出现异常,但没有能力处理这种异常,可以在方法声明处用throws子句来声明抛出异常。  

- throws意义：throws适用于分模块开发，由不同的开发人员分别进行方法定义和方法处理。  

比如：汽车在运行时可能会出现故障,汽车本身没办法处理这个故障，那就让开车的人来处理。  

#### 2. throws使用：  
> throws 声明的尽量是具体的异常, 而不是父类总异常 Exception

```java
public static void main(String args[]) {
        // 此处result调用了这个方法，由result处理方法抛出的异常
        try {
            int result = test();
            System.out.println(result);
        } catch (ArithmeticException e) {
            e.printStackTrace();
            System.out.println("=====除数不能为0=====");
        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.out.println("=====不能输入字母=====");
        }
    }

    private static int test() throws ArithmeticException,InputMismatchException{
        Scanner input = new Scanner(System.in);       
        int one = input.nextInt();      
        int two = input.nextInt();
        return one/two;
        }
```
[Throws声明异常类型，调用者处理--两数相除](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_02_Throws.java)  

#### 3. throws的继承  
当子类重写父类抛出异常的方法时,声明的异常必须是父类方法所声明异常的同类或子类。  

## 二. throw 抛出异常对象
> throw new IOException();

#### 1. throw 的特点  
1. 抛出异常的意义（我们为什么要主动用throw抛出一个异常对象？）  
  - 规避可能出现的风险  
  - 完成一些程序逻辑（比如未成年人进入网吧，抛出异常）  

2. throw 抛出的只能够是可抛出父类Throwable 或者其子类的实例对象，不能抛出其他类的实例对象。  
```java
throw new String(“出错啦”); //是错误的  
```

3. throw 抛出的尽量为**Excption/IOException/SQLException**等 checkedException 类型的异常，而不是uncheckedException类型，因为编译器对后者的约束较为宽松，写出来不报错，前者写出来会报错。

#### 2. throw 抛出异常后的处理方法 
> 两种方法: try catch 自己处理  
	    throws 抛出让调用者处理

 
自己处理:  

```java
public static void main(String args[]){
        testAge();
    }
    public static void testAge(){
            try {
                System.out.println("请输入年龄");
                Scanner input = new Scanner(System.in);
                int age = input.nextInt();

                if(age < 18){
                    throw new Exception("未成年人不能进入网吧");
                }else {
                    System.out.println("可以进入网吧");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
```
[Throw抛出异常对象自己抛出，自己处理--网吧年龄限制](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_03_Throw1.java)  

throws声明，让调用的方法处理:  

```java
public static void main(String args[]){
        try {
            testAge();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void testAge() throws Exception{
        System.out.println("请输入年龄");
        Scanner input = new Scanner(System.in);
        int age = input.nextInt();
        if(age < 18){
            throw new Exception("未成年人不能进入网吧");
        }else {
            System.out.println("可以进入网吧");
        }
    }
```
[Throw抛出异常对象自己抛出，Throws声明，调用者处理--网吧年龄限制](https://github.com/EasterFan/JavaExercise/blob/master/ExceptionProj/src/_04_Throw2.java)  

## 三. throws 和 throw 的区别
throws在函数名后, 可声明多个**异常类**,抛出的是异常类  
throw 在函数中, 抛出的是**异常对象**.
