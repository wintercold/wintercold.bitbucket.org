>我们同时打开浏览器/音乐/idea，每一个都是一个进程，进程是由线程组成的。  

## 一. 线程介绍
**多线程的意义：**  
开启线程的目的是为了让 CPU 运行我指定的代码.  
让操作系统能多任务进行操作。充分利用cpu资源。  
什么时候用多线程?  
当多个代码需要同时运行时,将多个代码块封装到线程里面,开启多个线程进行运行  
[多线程 + 匿名内部类的写法](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/ThreadSummary.java)  

**多线程的特性:**  
1. 随机性  
多线程程序,每一次运行的结果都不同  -- 因为多个线程都争夺CPU的执行权, CPU 执行到谁,谁就运行.  
明确一点,在某一时刻,只能有一个程序在运行.(多核除外)  
线程的执行具有随机性,谁抢到执行谁,至于执行多长时间,CPU说了算.  

**多线程注意事项**  
1. 在创建线程时, 就要明确,你想要运行的是哪段代码  
2. 被`syncniszed`修饰的函数是同步函数,当前对象(this)调用同步函数 --> 同步函数的锁是 this --> 两个同步方法(同步代码块 + 同步函数)应使用同一个锁(this)来保证线程安全  
[售票程序 -- 同步代码块 + 同步函数使用同一个锁](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/SynchronizedFunction.java)  
3. 静态同步函数的锁是**Class对象**,静态函数进入内存时,还没有对象(即没有this),只有类的字节码文件对象(类名.class)  
[静态同步函数的锁是class对象](https://github.com/EasterFan/JavaExercise/blob/master/ThreadProj/src/StaticSynchronizedFunction.java)  

## 二. 线程的创建
> 继承 Thread 类 或 实现 Runnable 接口(更常用)。  

两种方法的区别及场景：  
1.因为java不支持多继承，子类还需要继承其他父类，不能只继承Thread类时，就需要实现Runnable接口了。  
2.不打算重写Thread类的其它方法（继承意味着要重写父类所有方法）  
### 1. 创建一个Thread类,或者一个Thread子类的对象
> 继承 Thread 类,重写 run 方法  

1. 步骤  
  1. 子类覆盖父类中的run方法,将线程运行的代码存放在run中。
  2. 建立子类对象的同时线程也被创建  
  3. 通过调用**start**方法开启线程(一个对象只能将一个线程开启一次)  

2. 为什么要重写 run 方法?  
Thread 用于描述线程, 该类就定义了一个功能, 用于存储线程要运行的代码. 该存储功能就是 run 方法(线程启动后,要运行线程的代码, 这段代码存储在 run 方法中).  

3. `myThread1.start();`和`myThread1.run();`的区别??  
前者是开启线程并运行线程中的`run()`方法,每次运行后结果各不相同;  
后者是普通的方法调用,直接调用`run`方法,线程创建了,并没有运行,每次运行的结果都相同.  

```java
// 自定义一个线程类
class MyThread1 extends Thread{
    // 构造方法，调用父类的名字
    public MyThread1(String name){
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0;i<10;i++){
            System.out.println(getName()+"正在运行"+i);
        }
    }
}

public class ThreadTest1 {
    public static void main(String[] args) {
        System.out.println("主线程1");
        MyThread1 myThread1 = new MyThread1("子线程1");
        MyThread1 myThread2 = new MyThread1("子线程2");

	// myThread1.run();
        myThread1.start();
        myThread2.start();
        System.out.println("主线程2");
    }
}
```

第一个运行的是主进程1，其它两个进程和主进程齐头并进，什么时候获取CPU资源未知，故每次运行的结果都不相同。  

### 2. 创建一个实现Runnable接口的类的对象
实现接口的方法不能直接调用Thread类的方法，需要采取一些曲线救国的方法。  
比如获取线程名和启动线程。  

```java
class PrintRunnable implements Runnable{
    @Override
    public void run() {
        // 只能调用Thread类的静态方法
        System.out.println(Thread.currentThread().getName()+"正在运行");
    }
}
public class RunnableTest {
    public static void main(String[] args) {
        // 启动线程三步
        // 1.新建自定义线程类对象
        PrintRunnable printRunnable1 = new PrintRunnable();      
        // 2.将自定义线程类对象作为参数，新建Tread线程对象
        Thread thread1 = new Thread(printRunnable1);     
        // 3.通过Thread类的start方法启动线程
        thread1.start();    
    }
}
```
获取当前线程对象: `static Thread currentThread().getName`
