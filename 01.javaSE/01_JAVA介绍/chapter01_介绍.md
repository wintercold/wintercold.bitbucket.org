# JAVA 程序执行过程

![](../assets/markdown-img-paste-20180103190340172.png)  

java 编译器将 .java文件编译成 .class 字节码文件, JVM 将字节码解释执行 ,转换成具体平台上的机器指令, 实现一次编译,到处运行.  

# 几个重要概念
#### 1. JDK(java开发工具包)

JDK中有两大组件:  
- javac 编译器 -- .java 转为 .class
- java  解释器 -- 运行 .class 文件



#### 2. JRE(java 运行环境)  

- 包括 JVM , java 核心类库和支持文件.  
- 如果要运行编译后的 java 文件,需要安装 jre；如果要开发 java 程序,需要安装jdk     
- jdk 中附带有 jre

![](../assets/markdown-img-paste-2018010319172472.png)

#### 3. java 可以做什么

![](../assets/markdown-img-paste-20180103191902553.png)  

# Java 的组成成分
![](../assets/contain.png)
