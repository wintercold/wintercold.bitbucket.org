## java的六大数据类型
![](../assets/markdown-img-paste-20180103212412737.png)  
(整数默认:int  小数默认: double)  

## 数据类型的存储大小
![](../assets/markdown-img-paste-20180103212802654.png)

## 数值型 -- 整数
> 十进制,八进制,十六进制表示整型

- 八进制  
以 0 开头,包括 0-7 的数字.  
`037, 056`  

- 十六进制  
以0x开头,包括 0-9 和字母 a-f  
`0x12, 0xabcf, 0xabcff, 0xabcfL`  
十六进制后的L表示这是一个**长整型**  
`long a = 0xabcfL`

## 数值型 -- 浮点
> 默认一个浮点型数据是double类型

`float a = 1.234`报错  
`float a = 1.234f`正确  

科学计数法表示浮点型数据,如1.23*10^5  

```java
double d = 1.23E5;
float f = 1.23E5f;
```

## 字符型
> 整型和字符类型可以相互转换 --- ASCII码

ASCII码的作用,是为了将计算机传输的二进制整数000111转换成字母.  

但是这个整数范围为:[0-65535],如果在此范围之外,需进行强制类型转换 -- 不建议这样,会造成数据丢失

```java
char c = 'c';
char a = 65;
System.out.print(a);   // 输出 A
```

ASCII码只能将二进制转换成字母,后来出现Unicode编码,Unicode的目标是能够将二进制整数转换成世界上所有的字符.  

Unicode的表示法,是在值前加前缀**\u**+**4位十六进制**  
```java
char c = '\u005d';
System.out.print(c);   // 输出 [
```

转义字符:  

![](../assets/markdown-img-paste-20180104105213630.png)
在使用转义字符时,一定要有一个双引号,进行字符串拼接,如果全是转义字符,计算机会把所有的转义字符自动转换成整型数字.  

```java
int x=1, y=2;
System.out.println(x + '\t' + y + '\n');
System.out.println(x + "\t" + y + '\n');
System.out.println("" + x + '\t' + y + '\n');
```

## 基本数据类型的转换

- 自动类型转换

![](../assets/autoChange.png)
实线为无数数据损失的转换,虚线为可能发生数据丢失的自动转换.

- 强制类型转换
若A类型的数据表示范围比B类型大,则将A类型的值赋给B类型,需要强制类型转换.  

```java
// 整型转字符  自动与强转
char c1 = 65;
char c2 = (char)65536;


// long 转 float 自动转损失数据
float f1  = 102984762874647657283477L;
System.out.println(f1);    // 1.029847628e20

```
