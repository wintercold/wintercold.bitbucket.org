## 选择控制

> 多重if / 嵌套if / switch

if 有三种结构:  
1. if(){}
2. if(){}else{}
3. if(){}else if(){}else if(){}else{}

![](../assets/markdown-img-paste-20180104190233728.png)  

switch语句选择的类型只有四种:byte, short, int , char, String。  

![](../assets/markdown-img-paste-2018010419025486.png)  

如果判断的条件不多,且符合byte, short, int , char, String类型,推荐使用 switch.  
如果判断范围,涉及布尔值,推荐使用 if  

## 循环控制

> while  do-while for循环  循环嵌套

### 1.while

![](assets/markdown-img-paste-20180105171640502.png)

```java
int n = 1;   // 1. 循环变量n必须先初始化
 while (n < 5){   // 3. 表达式为true时, 语句才会被执行
            System.out.println(n);   // 语句
            n++;  // 2. 循环变量n必须改变
        }
```

### 2. do..while
> 不管表达式是否符合条件,循环都会被执行一次

![](assets/markdown-img-paste-20180105171509335.png)


### 3. for

![](assets/markdown-img-paste-20180105174817330.png)

```java
// 将10以下的值打印输出
        for(int i=1;i<=10;i++){
            System.out.print(i);
        }
```

- for循环的组成
for(初始化表达式;循环条件表达式;循环后的操作表达式)
{
执行语句;
}

```java
  int x = 0;
        for(System.out.println("a");x<3; System.out.println("c") ){
            System.out.println("b");
            x++;
        }
```

- for循环的执行顺序:  

![](assets/markdown-img-paste-20180105175357913.png)  
- 循环变量的作用范围:  
循环变量是局部变量,局部变量只在其所在的大括号内有效.

- for循环的三个表达式都是可省略的
第一种:省略int i
将int i拿到外面,可以扩大循环变量的作用范围.  
```java
// 将10以下的值打印输出
        int i=1;
        
        for(;i<=10;i++){
            System.out.print(i);
        }
```

第二种:省略判断条件
将判断条件放到里面.  

```java
// 将10以下的值打印输出

        int i=1;

        for(;;i++){
            System.out.print(i);
            if (i==10)break;
        }
```

第三种:省略i的自增  
将i++ 放到里面.  
```java
// 将10以下的值打印输出

        int i=1;

        for(;;){
            System.out.print(i);
            if (i==10)break;
            i++;
        }
```

这种情况下,可以极致的转为while  
```java
// 将10以下的值打印输出

        int i=1;

       // for(;;)
        while(true){
            System.out.print(i);
            if (i==10)break;
            i++;
        }
```

- for 和 while 的无限循环  
for(;;;){}  
while(true){}  

### 4.循环嵌套
> while 套while , do while 套while , for套 for


![](assets/markdown-img-paste-20180105223241460.png)


![](assets/markdown-img-paste-20180105223408963.png)

![](assets/markdown-img-paste-20180105223442673.png)


循环嵌套写阶乘时, `int sum`, 输出结果`-2664152777252537831` 发生溢出,
输出结果已经超出int范围,可以改为`long sum`或者使用java中的大数据类.

### 5.break和continue

- break语句后的代码不会被执行(有 if 逻辑也不行)
- break在多重循环中,只向外跳出一层,结束当前所有循环
- continue后的代码会被执行(有 if 逻辑即可)
- continue结束当前循环,执行下一次循环

多重循环中,通过给循环标号, 使 break/continue 跳出指定循环.  
```java
  out:for(int x=1;x<7;x++){
           in: for(int y=1;y<x;y++){
                System.out.println(y);
                if(y==4) break out;
            }
        }
```

[continue跳过当前循环]()
