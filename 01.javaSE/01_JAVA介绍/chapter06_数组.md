## 一. 创建数组

```java
// 声明数组:
   int arr[];
// 创建数组:
   int arr[] = new int[5];
//  初始化数组:
   int arr[] = {0,1,2,3,4};
```

声明并创建一个长度为10的整型数组.
与变量不同,我们在使用变量时,要先给变量赋值,才能使用;  
而数组本质上是一个**对象**,数组创建后都有默认值,整型数组的默认值为0.

## 二. 数组在内存中的存储
变量被赋值后,会在内存中存储,同样,创建数组后,数组也会在内存中存储.  
> 与变量不同,数组会被分配连续的内存空间

![](assets/markdown-img-paste-20180106112241893.png)  
例如,程序执行到这行代码时,会在内存中开辟一段长度为5的**连续的**内存空间,整型数组的默认值为0;  
**数组名a是一个对象, a 指向数组中第一个元素的内存地址.**
数组的下标从0开始.

## 三. 数组的使用
> 最值, 排序(选择排序,冒泡排序), 二分查找

### 1.循环为数组赋值--完成初始化
```java
int arr[] = new int[4];
// arr[] = {1,2,3,4};

for(int i=0; i<=4; i++){
   arr[i] = i + 1;
}
```

### 2.求数组元素最值
```java
        int max = arr[0];
        for(int i=0;i<arr.length;i++){
            if(max < arr[i]){
                max = arr[i];
            }
        }
```

### 3.增强型for循环遍历数组
```java
for (int i : arr) {  
}
```

### 4. 排序:选择排序和冒泡排序
![](../assets/bubble.jpg)  
冒泡排序和选择排序的低效在于:  
每次比较后在**堆**中交换位置,比较耗费资源；如果比较后将在堆中交换位置,改为在**栈**中交换位置,效率会高很多.  

代码实现:  
```java
// 冒泡排序
        // 最后一个元素不必遍历
        for(int i=0;i<arr.length -1;i++){
            // 内层循环,相邻的两个元素比较,每次循环减去前 n 个已经确定的元素, 且最后一个元素不必遍历
            for(int j=0;j < arr.length-i-1;j++){
                if(arr[j]>arr[j+1]){
                    changeTwoArrayElement(arr,j,j+1);
                }
            }
        }

        // 选择排序
        // 最后一个数组元素不用遍历
        for(int i = 0;i<arr.length-1;i++){
            // 前面比较过的小元素不需要再比较
            for (int j = i + 1;j<arr.length;j++){
                if(arr[i] > arr[j]){
                    changeTwoArrayElement(arr,i,j);
                }
            }
        }
```
[冒泡排序](https://github.com/EasterFan/JavaExercise/blob/master/basic/src/array/Bubble.java)  

然而,真实开发中,用`Arrays.sort()`进行数组排序.  

### 5. 二分查找
> 折半查找提高效率,但要求该数组必须为有序数组.

向一个有序数组中插入一个数,使得插入后的数组仍然是一个有序数组  
- 如果该数在数组中存在,插入到已存在数的下标中;如果不存在,返回二分查找的min值,插入.
```java
public static int getIndex(int arr[],int key){
        int min = 0;
        int max = arr.length - 1;
        int mid = (max + min) / 2;

        while (arr[mid] != key){
            if(key > arr[mid])
                min = mid + 1;
            else if(key < arr[mid])
                max = mid - 1;

            // 如果数组中无此元素,返回-1
            if(min > max) return -1;
            mid = (max + min) / 2;
        }

        return mid;
    }
```
[二分查找案例](https://github.com/EasterFan/JavaExercise/blob/master/basic/src/array/HalfSearch.java)  

### 6. 数组传参 -- 可变参数列表
当一个方法,需要向这个方法传入十几个参数时,  
```java
// 一个一个传, 很麻烦
public void show(int n1, int n2 ..... 省略十几个参数){}

// 把参数先存入数组中, 将数组作为参数传递给方法 -- 还要定义数组, 较麻烦
public void show(String arr[]){}

// 可变参数列表 -- 取代数组, 不需要自己定义数组, JVM帮你在内部自动封装
public void show(String ...str){}

// 可变参数要位于所有参数最后
public void show(int a, String ...str){}  
```
**注意:**  
当可变参数和普通参数一起传值时, 可变参数一定要定义在参数的最后面.  

[数组 - 可变参数列表](https://github.com/EasterFan/JavaExercise/blob/master/basic/src/function/ArgsChange.java)
## 四. 二维数组

### 1. 创建二维数组
> 三种方法创建

```java
// 创建二维数组
	int arr[][] = new int[3][4];
        int arr[][] = new int[3][];
        int[][] arr = {{3,8,2},{2,7},{9,0,1,6}};

```
注意特殊写法情况:int[] x,y[]; x是一维数组,y是二维数组。  

### 2. 二维数组在内存中的状态
```bash
+-------------------------+
|  +----+----+----+----+  |
|  |  3 |  5 |  2 |  6 |  |
|  +-------------------+  |
|  +----+----+----+----+  |
|  |  3 |  5 |  2 |  6 |  |
|  +-------------------+  |
|  +----+----+----+----+  |
|  |  3 |  5 |  2 |  6 |  |
|  +-------------------+  |
+-------------------------+
```
### 3. 二维数组的使用
二维数组累加和:  
```java
static int getTwoArraySum(int arr[][]){
        int sum = 0;
        for(int x = 0;x<arr.length;x++){
            for(int y=0;y<arr[x].length;y++){
                sum+=arr[x][y];
            }
        }

        return sum;
    }
```
