## 方法介绍
方法结构:  
![](assets/markdown-img-paste-20180106154425720.png)  

## 方法重载
> 方法名相同,参数列表不同(个数不同/数据类型不同/顺序不同),和返回值类型无关

[方法重载案例]()  
方法之间互相重载,直接方法名调用;  
在主方法中调用其他普通方法,先新建对象,然后用对象调用方法.
```java
// 方法
void show(int a, char b, double c){};
// 重载
void show(int a, double c, char b){};
// 不重载 -- 此函数不可与原函数在同一个类中
int show(int a, char b, double c){};
```
## 参数传值
> 对基本数据类型: 传递的只是参数值,对主函数中的变量值无影响
> 对数组和对象: 传递的是内存地址,普通函数的操作对数组/对象有影响

主函数中调用普通函数,进行参数传递时,主函数只是将自己的**变量的值**传给了普通函数,
无论普通函数对这个值进行怎样的操作,主函数中变量mn和mn对应的值(3,4)都不会改变,因为变量mn在内存中的地址没有改变,普通函数操作的是普通函数的内存地址.

```java
public class ChangeArge {

    public void swap(int a,int b){

        System.out.println("交换前ab:" + a + " " + b);

        int temp;
        temp = a;
        a = b;
        b = temp;

        System.out.println("交换后ab:" + a + " " + b);
    }

    public static void main(String[] args) {
        int m = 3, n = 4;
        System.out.println("交换前mn:" + m + " " + n);

        ChangeArge ca = new ChangeArge();
        ca.swap(m,n);
        System.out.println("交换后mn:" + m + " " + n);
    }
}
```
![](assets/markdown-img-paste-20180106193122533.png)

但是,当传递的是数组/对象时,普通函数的操作会对主函数中的数组/对象,产生影响.  

数组名arr存储的是一个地址,它指向数组内存空间第一个元素的内存地址.参数传递时,主函数传递的,是数组的内存地址,这样,普通函数和主函数,共同引用同一块内存地址,所以,普通函数的操作,会影响数组/对象的值.  


## 可变参数列表
> 可变参数列表和数组相似,可以将数组传给可变参数列表

- 可变参数列表实现累加和查找功能
- 可变参数列表的方法重载问题
**可变参数列表所在的方法是最后被访问的,优先级最低,只要有其他带参方法,程序优先调用带参方法**
