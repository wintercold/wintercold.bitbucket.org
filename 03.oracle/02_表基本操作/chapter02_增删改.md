> DML语句,伴随事务,包括增,删,改

## 增-insert
1.向表中插入数据  

```sql
INSERT INTO myuser
(name,password,gender,salary)
VALUES
('FDF','123456','W','5000');
```
注意:  
当你执行插入操作后,你可以看到插入后的预览数据,但是别人看不到,因为事务,将插入的数据写到了缓存中,并没有真正写入数据表中

2.向表中插入日期
```sql
INSERT INTO myuser
(name,password,time)
VALUES
('SS','DD',DATE_FORMAT('2017-01-01','%Y-%m-%d'));
```

## 删 - delete
不加`WHERE`限定范围,删除的是整张表

```sql
DELETE FROM myuser
WHERE name='FDF2';
```


## 改 - update
不加`WHERE`限定范围,更改的是整张表  

```sql
UPDATE myuser
SET salary=6000
WHERE name='dd';
```
