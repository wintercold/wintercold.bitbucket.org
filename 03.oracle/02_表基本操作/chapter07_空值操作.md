> TimeStamp 和 auto_increment ,往其中插入 null 值,则系统会插入一个正整数序列

## 空值操作

null 值的运算操作
null 与任何数字运算结果还是 null;  
null 与字符串拼接等于没有操作.

## 空值函数 ifnull

> 如果第一个参数是空值,就返回第二个,将空值 null 替换成指定值

```sql
mysql> select IFNULL(1/0,'yes');
        -> 'yes'
```
