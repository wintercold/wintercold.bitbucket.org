
## ROUND 四舍五入截取数字

```sql
SELECT ROUND(45.681 ,2) FROM dual;   -- 45.68
SELECT ROUND(45.678,0) FROM dual;   -- 46,保留到个位
SELECT ROUND(55.678,-1) FROM dual;  -- 60 ,保留到十位
```

## TRUNCATE(n,m) 直接截取数据,不四舍五入

```sql
SELECT TRUNCATE(45.681 ,2) FROM dual;   -- 45.68
SELECT TRUNCATE(45.678,0) FROM dual;   -- 45
SELECT TRUNCATE(55.678,-1) FROM dual;  -- 50
SELECT TRUNCATE(55.678,-2) FROM dual;  -- 0 百位数字本来就是 0 
```

## MOD 取余数

```sql
SELECT name,salary, MOD(salary,1000) FROM myuser;
```

## CEIL FLOOR 求最大/小整数

```sql
SELECT CEIL(45.12);  -- 45
SELECT FLOOR(45.12); --46
```
