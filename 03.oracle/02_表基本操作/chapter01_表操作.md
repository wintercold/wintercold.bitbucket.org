> DDL
# 创建表

## DEFAULT 语句
> 在创建表的时候可使用 default 为某个字段单独设定默认值

1. java 中基本数据类型默认值为 0 ,0.0,等,引用数据类型为null;  
数据库中,所有数据类型默认值都为 **null**  

2. 数据库中单引号表示字符串

```sql
CREATE TABLE user(
  id INT(4),
  name CHAR(20),
  birth DATE,
  gender CHAR(1) DEFAULT 'M'
  );
```


## NOT NULL
not null 是一种约束条件, 要求必须给值, 不能为空 

```sql
CREATE TABLE user(
  name CHAR(20) NOT NULL,
  password VARCHAR(40) NOT NULL,
  gender CHAR(1) DEFAULT 'M'
);
```

# 修改表
## 修改表名

```sql
ALTER TABLE user RENAME TO myuser;
DESC myuser;
```

## 修改表结构
> 对表结构的修改尽量在表中没有数据的时候修改；若表中有数据,尽量避免修改字段类型

- 1.添加新字段  
添加的新字段只能被追加在原表的末尾, 不能在表在中间插入

```sql
ALTER TABLE myuser
ADD (
  regtime DATE 
);
```

- 2.修改现有字段  
可以修改字段**类型/长度/默认值**,字段名不能改

```sql
ALTER TABLE myuser
MODIFY
  name CHAR(40) DEFAULT 'MING';
```

- 3.删除现有字段  

```sql
ALTER TABLE myuser
DROP regtime ;
```
