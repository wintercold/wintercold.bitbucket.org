> 从多张表中查询对应记录的信息, 关联查询的重点在于这些表中记录的对应关系, 这个关系叫做**连接条件**

```sql
-- 关联查询
SELECT b.rkid,e.kcdmc,d.rqss,d.rqxh
FROM instoreb b,instored d ,storeplace e 
WHERE b.rkid = d.rkid;

-- 产生笛卡尔积的查询 -- 没有连接条件
SELECT b.rkid,e.kcdmc,d.rqss,d.rqxh
FROM instoreb b,instored d ,storeplace e;
```

关联查询要添加**连接条件**,否则会产生**笛卡尔积**,笛卡尔积通常是一个无意义的结果集,它的纪录集是所有参与查询的**表的记录数乘积**的结果.  

要避免出现,数据量大时极易出现内存溢出,宕机等现象.  
(淘宝的买家卖家如果没有对应连接,出现一次笛卡尔积,就要宕机)

## 如何避免笛卡尔积
> n 张表进行关联查询, 必须至少有 (n-1) 个关联条件


## 内连接
> 内连接和普通关联查询的功能是一样的, 写法不一样

1. 内连接的好处: 将连接条件和过滤条件分开, 很直观
2. 有人说, 内连接查询速度比关联查询速度更快,因为有些数据库内部做了优化, 会默认将关联查询转为内连接, 这样,关联查询速度相对变慢了


```sql
-- 关联查询 -- 过滤条件 和 连接条件 堆在一起
SELECT b.rkid,e.kcdmc,d.rqss,d.rqxh 
FROM instoreb b,instored d ,storeplace e
WHERE b.rkid = d.rkid 
AND b.rkly = e.kcdid
AND substring(b.rkid,1,6) >= ?
AND substring(b.rkid,1,6) <= ? ;

-- 内查询 -- 过滤条件 和 连接条件 分开
SELECT b.rkid,e.kcdmc,d.rqss,d.rqxh 
FROM instored d JOIN instoreb b 
ON b.rkid = d.rkid 
JOIN storeplace e
ON b.rkly = e.kcdid
WHERE substring(b.rkid,1,6) >= ?
AND substring(b.rkid,1,6) <= ? ;
```

## 外连接
> 外连接除了会将满足连接条件的记录查询出来, 还会将**不满足**连接条件的记录也查询出来,用 NULL 填充.  

### 左外连接
以 JOIN 左侧作为驱动表(所有数据都会被查询出来), 那么,当该表中某条记录不满足连接条件时, 来自右侧表中的字段全部填 NULL.

```sql
-- 标准左外连接
SELECT b.rkid,d.rqxh
FROM instoreb b LEFT OUTER JOIN instored d 
ON b.rkid = d.rkid;

-- 内连接实现的左外连接
SELECT b.rkid,d.rqxh
FROM instoreb b JOIN instored d 
ON b.rkid = d.rkid(+);


```

### 右外连接
以右侧表为驱动表, 左侧表填 NULL

```sql
-- 标准右外连接
SELECT b.rkid,d.rqxh
FROM instoreb b RIGHT OUTER JOIN instored d 
ON b.rkid = d.rkid;

-- 内连接实现的右外连接
SELECT b.rkid,d.rqxh
FROM instoreb b JOIN instored d 
ON b.rkid(+) = d.rkid;
```

## 全连接
哪边没有,哪边填 NULL.
```sql
SELECT b.rkid,d.rqxh
FROM instoreb b FULL OUTER JOIN instored d 
ON b.rkid = d.rkid;
```

## 自连接
> 用于存储同类型数据, 数据间存在上下级关系

1. 自连接定义  
当前表的一条记录对应当前表自己的多条记录

2. 自连接的使用场景  
用一张表存储一个可无限扩展的树状结构(存在上下级关系),比如在商品表,一张表中存储所有商品,公司职员表,上下级关系    


|id  |name|pid |
|----|----|----|----|
|1   |食品  |    |    
|2   |服装  |    |    
|3   |电子产品|    |    
|4   |水果  |1   |    
|5   |蔬菜  |1   |
|6   |衬衫  |2   |
|7   |裙子  |2   |
|8   |女士衬衫|6   |
|9   |男士衬衫|6   |

自连接查询:  

```sql
-- 查看商品编号为 8 的商品名称和其父商品名称
SELECT p1.name,p2.name
FROM product p1 JOIN product p2
ON p1.id = p2.pid
WHERE p2.id = 8;
```
