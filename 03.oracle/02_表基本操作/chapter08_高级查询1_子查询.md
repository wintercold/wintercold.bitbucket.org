# 子查询
> 定义 , 分类 , 三种语句下的使用场景(DDL DML DQL [WHERE , HAVING , FROM, SELECT])

1. 子查询是一条 SELECT 语句,但它是嵌套在其他SQL语句中的, 为的是给该 SQL **提供数据**以支持其执行操作. 子查询并不是你想要查询的数据, 但是你要拿到它,用子查询的数据做工具去查询你想要的数据.  
2. 子查询可以在 DDL DML中使用,但最常用的,还是在 DQL 查询中使用
3. 子查询最常用于**SELECT**语句中

## 子查询的分类
> 根据**查询结果集**的不同, 分成三类

### 1.单行单列子查询 1 X 1
> 单行单列常用到的关键字 :  =,>,< 等比较运算符

常用于过滤条件, 常配合 =,>,< 等比较运算符使用M
```bash
# 单行单列的形式
+--------+
| gender |
+--------+
| M      |
+--------+
```

### 2.多行单列子查询 1 X N
> 多行单列常用到的关键字 : IN  ANY  ALL EXISTS

1. 常用于过滤条件, 由于查询出多个值,   
在判断 = 时要用 IN,   
判断 > , >= 等操作要配合 ANY ALL

2. 子查询是真正使用 ANY ALL的地方

3. ANY 是比所有的数, EXISTS 是指, 该子查询可以查询出至少一条记录时, EXISTS 返回 true, 直到 EXISTS 返回值为 false, 查询停止


查询与员工 ddd 同性别的其他员工的姓名

```bash
# 多行单列的形式
+--------+
| gender |
+--------+
| W      |
| W      |
| M      |
+--------+
```

```sql
-- 子查询出来的结果集是多行单列形式
-- 用下面 = 的写法报错
SELECT name,gender
FROM myuser
WHERE gender = (SELECT gender 
                FROM myuser 
                WHERE name = 'ddd');
                
-- 以下为正确写法
SELECT name,gender
FROM myuser
WHERE gender IN (SELECT gender 
                FROM myuser 
                WHERE name = 'ddd');
  
  -- PS:这个例子举的不太好,关键在于 select 子查询出的结果集是一个多行单列的形式, 是这个意思 
  
  
SELECT name,salary
FROM myuser
WHERE salary > ALL(SELECT salary 
                FROM myuser 
                WHERE name = 'ddd');  
                
+--------+
| salary |
+--------+
| 5000   |
| 1452   |
| 2542   |
+--------+ 

-- 下面这种exists用法相当于where 始终为true

SELECT name,salary
FROM myuser
WHERE EXISTS (SELECT salary 
                FROM myuser 
                WHERE name = 'ddd');  
```

### 3.多行多列子查询 N X N
常当做一张表看待.


## 子查询 -- WHERE
比如,查看所有分数高于小明同学分数的学生姓名

小明同学的分数就是一个子查询  

```sql
SELECT name 
FROM myuser2
WHERE salary > (SELECT salary FROM myuser2 WHERE name = 'ddd');
```

## 子查询 -- HAVING
> WHERE 和 HAVING 都是用来过滤的, HAVING 用来过滤分组

查看每个部门的最低薪水, 前提是该部门的最低薪水要高于30号部门的最低薪水

```sql
SELECT deptno, MIN(salary) AS min_salary
FROM myuser
GROUP BY deptno
HAVING MIN(salary) > (SELECT MIN(salary) 
                      FROM myuser 
                      WHERE deptno=30);
```


## 子查询 -- FROM
> 用from, 说明你将子查询的结果集当成一张表看待, 基于这张表进行二次查询

1. 当一个子查询结果集是多列纪录集时, 通常将该结果集看作一张表
2. 如果要在一个子查询的结果中继续查询, 则子查询出现在 FROM 语句中, 这个子查询也称为行内视图, 或者匿名视图
3. 把子查询当作视图对待, 但视图没有名字, 只能在当前的 SQL 语句中有效

一个公司有很多部门, 一个部门有很多员工,  
现在,查询所有员工姓名, 工资, 部门号,   
前提是这些员工分别是他们自己所在部门的佼佼者,   
他们的工资高于其所在部门的平均工资.  

```sql
-- 第一步: 子查询, 查询公司所有部门的平均工资 -- 结果集是一张表

-- 第二步 : 在这张表中取数据, 进行二次查询

SELECT e.ename, e.salary, e.deptno
FROM employ e, (SELECT AVG(salary),deptno
                FROM employ
                GROUP BY deptno) t
WHERE e.deptno = t.deptno          -- 连接条件
AND e.salary > t.avg_salary;       -- 过滤条件

```

## 子查询 -- SELECT
> 子查询结果是单行单列, 把结果集当做 select 的一个属性 , 用途相对较少

```sql
SELECT e.ename,e.salary,
    (SELECT d.dname 
     FROM dept d
     WHERE d.deptno = e.deptno) AS deptno
FROM employ e;
```

## 在 DDL 中使用子查询
> 可以根据子查询的**结果集**快速创建一张表, 

1. 通过这种方法,可以对表中数据实现截取子表;整张表的转移;将两张表合成一张表
2. 当子查询中一个字段含有函数或表达式, 那么该字段必须给别名, 新表中对应的字段名为子查询中的别名
```sql
CREATE TABLE myuser2
AS
SELECT name,password,gender,salary
FROM myuser;
```

## 在 DML 中使用子查询
删除与FDF相同性别的人  
```sql
DELETE FROM myuser
WHERE gender = (
                SELECT gender FROM(
                  SELECT gender FROM myuser WHERE name = 'eee'
                ) AS temp 
              );
```




# DECODE 函数
---

## DECODE 函数基本语法

## DECODE 函数中分组查询的应用

# 排序函数
---
> ROW_NUMBER  RANK  DENSE_RANK

# 高级分组函数
---
> ROLLUP  CUBE  GROUPING SETS

# 集合操作
---
> UNION ,  UNION ALL , INTERSECT  , MINUS
