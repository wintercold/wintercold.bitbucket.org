## sysdate() -- 系统时间
> sysdate 可以被 sleep 方法延时,now 不会被延时


## now() -- 当前时间
> sysdate 和 now 都精确到秒


## curdate() -- 当前时间
> 精确到天


## 日期和字符串相互转换的方法

- date 转 char
```sql
mysql> SELECT DATE_FORMAT('2009-10-04 22:23:00', '%W %M %Y');
        -> 'Sunday October 2009'
```

- str 转 date
```sql
mysql> SELECT STR_TO_DATE('01,5,2013','%d,%m,%Y');
        -> '2013-05-01'
```

## 日期函数

### LAST_DAY(date) 返回日期date所在月的最后一天

```sql
mysql> SELECT LAST_DAY('2003-02-05');
        -> '2003-02-28'
```



### 比较函数 LEAST GREATEST
> 比较两个日期的大小 , 比较之前,第二个参数以后的参数会被转换为第一个参数的数据类型, 如果不能转换,则报错

```sql
mysql> SELECT LEAST(CURDATE(),'1992-12-10');
+-------------------------------+
| LEAST(CURDATE(),'1992-12-10') |
+-------------------------------+
| 1992-12-10                    |
+-------------------------------+
1 row in set (0.00 sec)

```

### extract()提取给定日期中指定时间分量的值

```sql
 SELECT EXTRACT(YEAR FROM '2010-03-11');
```
