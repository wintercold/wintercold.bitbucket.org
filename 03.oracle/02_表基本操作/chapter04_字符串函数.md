## concat 拼接字符串

```sql
SELECT CONCAT(name,'+',salary)
FROM myuser;
```

## length 查看varchar长度

```sql
SELECT name,LENGTH(password)
FROM myuser;
```

## upper lower initcap 字符串大小写转换
```sql
SELECT name,UPPER(password)
FROM myuser;
```

## 伪表
为了满足语法要求,查询一个假表**dual**  

```sql
SELECT UPPER('dfdg')
FROM dual;
```

## 截单字符
> TRIM  LTRIM  RTRIM

```sql
SELECT TRIM(BOTH 'x' FROM 'xxxbarxxx');
```

## LPAD / RPAD 补位函数
> 实现左对齐,右对齐效果

```sql
SELECT LPAD(salary,3,'$')
FROM myuser;
```

## SUBSTR 截字串
> java 下标从 0 开始, 数据库下标从 1 开始

从第 13 个字符开始截,往后截 4 个字符, 13 取值可以为负数

```sql
SELECT SUBSTR('thinking in java',13,4)
FROM dual;
```

## INSTR(char1,char2)
> 返回字串2在字串1中的位置

```sql
SELECT INSTR('thinking in java','java')
FROM dual;
```
