# 1. 基础查询

## LIKE 模糊查询
> 模糊匹配字符串

两个通配符:  
_ : 单一的字符  
% : 任意个字符  

```sql
SELECT name FROM myuser
WHERE name LIKE '_%F';
```

## IN / NOT IN
> 判断是否在列表中,常用来判断子查询的结果

```sql
SELECT name 
FROM myuser
WHERE gender IN ('M','W');
```

## between and
> 可用来比较数值,也可以比较日期

```sql
SELECT name , salary
FROM myuser
WHERE salary BETWEEN 1000 AND 10000;
```

## any all
> any 和 all 不能单独使用,要配合大于小于号, 常用来判断子查询

```sql
SELECT name, salary
FROM myuser
WHERE salary > ANY_VALUE(100,1200,200);
```

## DISTINCT
> 结果集去重

```sql
SELECT DISTINCT name , salary
FROM myuser;
```
distinct 多字段去重原则:  
多字段组合没有重复的  

# 2. 排序 -- ORDER BY
> 位置必须出现在 SELECT 中的最后一个字句

```sql
SELECT name, salary,gender
FROM myuser
ORDER BY name DESC;
```
1. 默认升序 , desc 降序 
2. order by可以根据多个字段排序, 排序规则: 先按照第一个字段排序,第一个字段相同的情况下,才会进行第二个字段的排序 ；如果第一个字段没有重复值,根本不会排第二个字段
3. 每个字段都可以单独制定是按照升序还是降序排列
4. 如果排序中出现 null 值, null 默认是最大值

# 3. 聚合函数
> 聚合函数是用来统计的, 把结果集中的数据分析,最后出一个结果

如: 查看公司员工月薪的最大最小值  
```sql
SELECT MAX(salary),MIN(salary),AVG(salary),COUNT(name)
FROM myuser;
```
1. 常见的聚合函数:  
`MAX(),MIN(),AVG(),SUM()` 
2. 对记录数的统计:  
`COUNT(),COUNT(*)`
3. 所有聚合函数都是忽略 NULL 值统计
4. 如果想要加上 NULL ,可以这样写
```sql
SELECT COUNT(IFNULL(salary,0))
FROM myuser;
```

5. **WHERE 中不能使用聚合函数作为过滤条件**  
错误示例:  
```sql
SELECT MAX(salary)
FROM myuser
WHERE MAX(salary) > 3000;
```
WHERE 在检索数据表时,只过滤一遍,然后筛选符合过滤条件的记录, 放到结果集中, 在WHERE 中使用聚合函数的矛盾在于:
WHERE 只检索一遍, 第一遍检索的时候,并不知道最大值是多少.  

使用聚合函数的结果作为过滤条件, 那么一定是数据从表中查询完毕(WHERE 在查询过程中发挥作用),得到结果集, 并且分组完毕, 才进行聚合函数统计结果,由此可见,聚合函数的过滤时机是在 WHERE 之后进行的.  

综上:  
> 1. WHERE 语句在查询过程中发挥作用, 聚合函数在查询结束,且分组结束之后发挥作用 , 故在WHERE语句中使用聚合函数, 作用顺序本末倒置

> 2. 聚合函数不能作为 WHERE 语句的过滤条件


# 4. 分组 -- GROUP BY
> 分组是为了将数据细分,配合聚合函数使用,如果没有聚合函数,就不要GROUP BY 分组了

1. 使用场景  
一张表中有整个公司的工资,我们用聚合函数, 只能用 AVG 计算整个公司的平均工资, 如果我想要计算一个部门的平均工资呢, 这就用到了**分组函数**

2. 分组函数的作用
**分组函数**是配合**聚合函数**使用的,分组函数的作用就在于数据的**细分**.  

3. 分组的过程
将查询出来的结果集, 按照一个指定的字段值, 将该字段值对应的记录, 分成一组, 配合聚合函数, 进行组内计算

4. GROUP BY也可以根据多个字段分组  
多字段分组原则:    
**将这几个字段都相同的记录看作一组**  

```sql
-- 查看公司女员工的平均工资
SELECT AVG(IFNULL(salary,0)),gender
FROM myuser
GROUP BY gender;


-- 查看男女分组中最高工资
SELECT MAX(salary),gender
FROM myuser
GROUP BY gender;
```

5. 过滤分组 -- HAVING  
我们已经查到 **男女分组中的平均工资** ,如果想要查找**男女分组中平均工资大于2500**的分组呢?
 
  - 聚合函数的过滤条件要在 **HAVING**子句中使用
  - HAVING 必须跟在 **GROUP BY**子句之后
  - HAVING 是用来过滤分组的
  - HAVING 在聚合函数发挥作用后,再进行分组过滤的

```sql
-- 查看公司男女员工的平均工资 , 并在两组中筛选平均工资大于2500的分组

SELECT AVG(IFNULL(salary,0)),gender
FROM myuser
WHERE AVG(IFNULL(salary,0)) > 2500
GROUP BY gender;

-- 显然上面的做法是错误的, 因为 WHERE 语句中不能使用聚合函数作为过滤条件


-- 下面是正确写法: 使用 HAVING 对分组进行过滤
SELECT AVG(IFNULL(salary,0)),gender
FROM myuser
GROUP BY gender
HAVING AVG(IFNULL(salary,0)) > 2500;
```
关于where group by,聚合函数的问题,实质上是查询语句执行顺序的问题,[详见博文](http://easterpark.me/2018/01/16/sql-select-sequence.html)   

6.关于写 GROUP BY 的一个小技巧  
> GROUP BY 后的字段与 SELECT 后的字段必须对应

```bash
                  +-------------+   
SELECT AVG(score),|major,name,id|
                  +-------------+
FROM myuser
         +--------------+
GROUP BY |major,name,id;|
         +--------------+
```
