> SQL 语言分为 DDL DML TCL DQL  DCL

## 数据定义语言 DDL 
> 对数据库对象进行操作

- Data Definition Language
- 建立, 修改, 删除 数据库对象(表,视图,索引,序列)
- 包括:  
  - CREATE : 创建对象
  - ALERT : 修改对象
  - DROP : 删除对象
  - TRUNCATE : 删除表数据, 保留表结构
truncate直接删除,delete可回滚,truncate效率更高.

## 数据操作语言 DML

> 对对象中的数据进行操作, DML 的执行自动伴随 TCL 语句执行

- Data Manipulation Language
- 用于改变数据表中的数据
- 和事务相关, 执行完后,需要经过事务控制语句提交后才真正将改变应用到数据库中
- 包括:  
  - INSERT : 将数据插入到数据表
  - UPDATE : 更新数据表中已存在的数据
  - DELETE : 删除数据表中的数据


## 事务控制语句 TCL
> 事务的作用 , 给你一个后悔的机会

- Transaction Control Language
- 用来维护数据一致性的语句
- 包括:  
  - COMMIT : 提交 -- 确认已经进行的数据改变
  - ROLLBACK : 回滚 -- 取消已经进行的数据改变
  - SAVEPOINT : 保存点 -- 使当前的事务可以回退到指定的保存点, 相当于做快照

1. 事务是伴随着 DML 语句进行的
2. 事务的开启是自动的
3. Rollback 和 commit 都会导致事务的结束

## 数据查询语言 DQL
> 查询表中的数据

- Data Query Language
- 用来查询表中的数据
- SELECT 语句
- 查询是最复杂的


## 数据控制语言 DCL
> 权限操作, 管理数据库

- Data Control Language
- 用于执行权限的授予和收回操作
- 包括:  
  - GRANT : 授予权限
  - REVOKE : 收回已有的权限
  - CREATE USER : 创建用户
