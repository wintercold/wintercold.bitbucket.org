## 一. 什么是约束

#### 1.什么是约束
约束是在数据表上强制执行的一些数据校验规则.  

对数据做合法性校验,当执行 DML 操作时,数据必须符合这些规则, 如果不符合则无法执行.  

#### 2.约束的意义
约束条件可以保证表中数据的完整性, 保证数据间的商业逻辑

#### 3. 约束的类型
> 约束条件有 5 个

- 非空约束 -- (NOT NULL) -- NN
- 唯一性约束 -- (Unique) -- UK
- 主键约束 -- (Primary KEY) -- PK
- 外键约束 -- (Foreign Key) -- FK
- 检查约束 -- (Check) -- CK

## 二. 非空约束
> 添加和删除非空约束

```sql
-- 1.建表时添加非空约束

CREATE TABLE user_test(
  name CHAR(6),
  password VARCHAR(20) NOT NULL
);

-- 2.修改表时添加非空约束
ALTER TABLE user_test
MODIFY 
  name VARCHAR(10) NOT NULL;
  
-- 3.取消非空约束
ALTER TABLE user_test
MODIFY 
  name VARCHAR(10) NULL;
```

## 三. 唯一约束
> 保证表中该字段的值, 任何一条记录都不重复, NULL 除外

```sql

-- 1. 添加 - 建表时两种方式创建唯一约束
CREATE TABLE user_test1(
  name CHAR(6) UNIQUE,
  password VARCHAR(20),
  email CHAR(20),
  createTime DATE,
  CONSTRAINT user_test1_email_uk UNIQUE(email)
);

-- 2. 添加 - 建表后
ALTER TABLE user_test1
ADD CONSTRAINT user_test1_password_uk UNIQUE(password);
```

## 四. 主键约束
> 主键 = 非空 + 唯一

### 1.主键的意义
1. 主键可以是单字段,也可以是多字段的组合
2. 一个表上只允许建立一个主键
3. 主键可以在表中唯一地确定一行记录

### 2.主键选取的原则
- 一张表中第一列是主键,一般名为 id
- 主键应是对系统无意义的数据
- 永远也**不要更新主键**,让主键除了唯一标识一行以外,再无其他的用途,就像身份证号
- 主键应自动生成,不要人为干预
- 主键不应包含动态变化的数据,如时间戳
- 主键应尽量建立在单列上

### 3.添加主键约束

```sql
-- 创建主键
CREATE TABLE user_test3(
  uid INT(6) PRIMARY KEY,
  name CHAR(3),
  password VARCHAR(5)
);

-- 添加主键
ALTER TABLE user_test1
ADD CONSTRAINT user_test1_password_uk PRIMARY KEY(name);
```
## 五. 外键约束
> 工作中基本上不使用外键约束, 已经被抛弃

### 1.外键约束的意义
1. 外键在**关联关系**中起作用
2. 外键约束定义在两张表的字段,或一张表的两个字段上,用于保证相关两个字段的关系

### 2.添加外键约束
```sql
-- 创建主键
CREATE TABLE user_test4(
  uid INT(6) PRIMARY KEY,
  name CHAR(3),
  password VARCHAR(5)
);

-- 添加外键
ALTER TABLE user_test4
ADD CONSTRAINT user_test4_uid_pk FOREIGN KEY(uid) REFERENCES user_test3(uid);
```

### 3.外键约束对一致性的维护
 子表的外键参照父表的主键,外键的麻烦之处在于:  
 
 添加外键后,对子表和父表都产生了约束:  
  - 子表添加的记录,必须在父表中已存在
  - 父表删除记录,必须保证该记录在子表中没有被引用,或者为 NULL

### 4.外键约束对性能的降低
- 如果在一个频繁 DML 操作的表上建立外键, 每次 DML 操作,都将导致数据库自动对外键所关联的对应表做检查, 产生开销
- 外键约束限制了父子表生成的顺序, 对某些业务造成限制

### 5.关联不一定非要外键约束
- 保证数据完整性可由程序或触发器控制
- 简化开发, 维护数据时不用考虑外键约束
- 大量数据 DML 操作时不需考虑外键耗费时间

## 六. 检查约束

### 1.检查约束的意义
对某个字段的取值添加更详细的取值范围

### 2.添加检查约束
```sql
-- 员工的薪水必须大于 2000
ALTER TABLE user_test4
ADD CONSTRAINT user_test4_salary_check CHECK (salary > 2000);
```
