## 什么是序列
> 未完成,序列在 oracle 中使用, mysql 中没有序列

> 序列是数据库对象,序列就是一系列数字, 为主键提供 id

mysql 通过 AUTO_INCREMENT 实现自增长,  
mysql 没有序列, oracle 有序列概念  

## 创建序列

```sql 
CREATE SEQUENCE sequence_name
[START WITH i] [INCRE
MENT BY j]
[MAXVALUE m] [NOMXVALUE]
[MINVALUE n | NOMINVALUE] 
[CYCLE | NOCYCLE] [CACHE p | NOCACHE]
```

## 使用序列


## 删除序列
```sql 
DROP SEQUENCE sequence_name;
```
