> 给表加索引的作用, 在于提高查询效率.

## 索引的原理
1. 索引就像字典的目录.  
索引是加在字段上的.

2. 索引的统计与应用,是数据库自动完成的,  
只要数据库认为可以使用某个已创建的索引时,  
数据库就会自动使用改索引,我们要做的,就是告诉数据库哪个字段需要加索引.  

## 创建索引

```sql
-- 创建单列索引
CREATE INDEX idx_myuser 
ON myuser(name);

-- 创建复合索引
-- 如果经常在 ORDER BY 字句中使用 name和salary 作为排序依据, 可以建立复合索引
CREATE INDEX idx_myuser2 
ON myuser(name,salary);

-- 做下面查询时, 会自动应用刚刚建立的索引
SELECT password,gender,salary,name 
FROM myuser 
ORDER BY name,salary;
```

## 创建基于函数的索引
如果需要在 myuser 表的 name 列上进行大小写无关搜索, 可以在此列上建立一个基于 UPPER 函数的索引:  

```sql
CREATE INDEX idx_myuser3 
ON myuser(UPPER(name));


-- 做下面查询时, 会自动应用刚刚建立的索引

SELECT *
FROM myuser
WHERE UPPER(name) = 'MING';

```

## 修改和删除索引
如果经常在索引列上进行 DML 操作, 需要定期重建索引, 提高索引的空间利用率:  

```sql
-- 修改索引
ALTER INDEX idx_myuser REBUILD;

-- 删除索引
DROP INDEX idx_myuser;
```
## 合理使用索引提升查询效率
1. 不要在小表(<10000条)上建立索引
2. 为经常出现在 WHERE 字句中的列创建索引
3. 为经常出现在 ORDER BY DISTINCT 后面的字段创建索引. 如果建立的是复合索引, 索引的字段顺序要和这些关键字后面的字段顺序一致.  
4. 为经常作为表的连接条件的列创建索引
5. 不要在经常做 DML 操作的表上建立索引
6. 限制表上的索引数目, 索引并不是越多越好
7. 删除很少被使用的, 不合理的索引
