## 一. 什么是视图

### 1.视图定义
1. 视图是一条 SQL 语句的查询结果集,把结果集看成一张表
2. 视图不是真实存在的表, 而是一张虚表, 是一组数据的逻辑表示
3. 视图本身并不包含任何数据,它只包含一个**映射**到基表的查询语句, 当基表数据发生变化时, 视图数据也随之变化
4. FROM 语句的子查询就是一张**内视图**,将子查询的结果集当作一张表,为 FROM 语句提供数据
5. 内视图和外视图的区别在于: SQL 语句的重用
6. **视图不是表**,视图只是一个子查询结果集,只是把它当作一张表,可以像操作一张表一样操作视图,主要是查询 

### 2. 视图的意义
- 如果需要经常执行某项复杂查询,可以基于这个复杂查询建立视图, 此后查询此视图即可, **简化复杂查询**

- 视图本质上就是一条 SELECT 语句, 所以访问视图时,只能访问到所对应的 SELECT 语句中涉及到的列 , 对基表中其他列起到安全和保密的作用, **限制数据访问**

### 3. 视图分类
> 根据视图所对应的**子查询种类**

- SELECT 语句是基于单表建立的, 且不包含任何函数计算 / 表达式 / 分组函数 , 叫做**简单视图**,此时视图是基于基表的子集.  
- SELECT 语句同样是基于单表建立的 , 但包含了单行函数 / 表达式 / 分组函数 / GROUP  BY 字句 , 叫做**复杂视图**
- SELECT 语句是基于多个表的, 叫做**连接视图**

## 二. 创建视图
```sql
-- 1. 用户必须有 CREATE VIEW 系统权限,才能创建视图
GRANT CREATE VIEW TO easter
```
### 1.单表视图

```sql
-- 2. 创建一个简单视图显示员工姓名 性别 薪水

CREATE VIEW v_test
AS

SELECT name,gender,salary
FROM myuser;

-- 3. 查看视图结构
DESC v_test;

-- 4. 查看视图数据
SELECT * FROM v_test;
```

### 2.创建带有 CHECK OPTION 约束的视图
```sql
CREATE OR REPLACE VIEW v_test1
AS
SELECT name,password,salary,gender
FROM myuser
WHERE gender='M'
WITH CHECK OPTION;
```
### 3.创建具有 READ ONLY 约束的视图
> 未验证, mysql 不支持read only 字段
```sql 
CREATE OR REPLACE VIEW v_test2
AS
SELECT name,password,salary,gender
FROM myuser
WITH READ ONLY;
```

### 4.通过查询 USER_VIEWS 获取相关信息
> 未验证, mysql 数据字典异于 oracle

> 和视图相关的数据字典: USER_OBJECTS  USER_VIEWS  USER_UPDATE_COLUMNS

```sql
-- 在数据字典 USER_OBJECTS 中查询所有视图名称

SELECT object_name  
FROM user_objects
WHERE object_type = 'VIEW'
AND object_name LIKE '%_easter';

-- text字段表示 视图所对应的 sql 语句
SELECT TEXT,view_name
FROM user_views;

-- 查看这个数据库中曾经建立的表
SELECT table_name
FROM user_tables;

```

### 5.创建复杂视图(多表关联)
创建一个含有公司部门工资情况的视图,内容为:  
部门编号, 部门名称, 部门的最高 / 最低 / 平均 / 工资总和 信息.

```sql 
CREATE VIEW v_multi
AS

SELECT d.dname,d.deptno,
       MIN(e.salary) min_salary,
       MAX(e.salary) max_salary,
       AVG(e.salary) avg_salary,
       SUM(e.salary) sum_salary
FROM emp e,dept d
WHERE e.deptno = d.deptno
GROUP BY d.deptno,d.dname

```
结合以上视图,  
查看谁比自己所在部门的平均工资高?  

```sql
SELECT e.salary ,e.name,e.deptno
FROM employ e, v_multi v
WHERE e.deptno = v.deptno
AND e.salary > v.avg_salary;
```
## 三. 视图操作

### 1. 修改视图
> 由于视图仅对应一个 SELECT 语句,所以修改视图就是替换该 SELECT 语句

```sql
-- 修改原视图为: 显示员工姓名 年薪

CREATE OR REPLACE VIEW v_test
AS

SELECT name,salary * 12 year_salary
FROM myuser;

```
### 2. 查询视图
```sql
-- 1. 查看视图结构
DESC v_test;

-- 2. 查看视图数据
SELECT * FROM v_test;
```

### 3. 对视图进行 DML 操作
- 对视图进行 DML 操作就是对视图数据来源的基表进行的操作
- 仅能对简单视图进行 DML 操作, 复杂视图和连接视图不能进行 DML 操作
- 对视图的 DML 操作,就是对基表的操作, 操作不当, 会对基表产生数据污染--比如视图插入 / 更新了一条自己看不见的数据. 
- 删除不会产生污染 -- 因为视图只能删除视图能看见的数据, 不能删除基表中自己看不见的数据

#### 3.1 DML -- 增删改操作

```sql
-- 1. 创建视图
CREATE OR REPLACE VIEW v_user_insert
AS

SELECT name,password,gender,salary
FROM myuser;

-- 2. 插入视图
INSERT INTO v_user_insert
(name,password,gender,salary)
VALUES
('view','view','M',5200);

-- 3. 更新视图 -- 将姓名改为 jackView
UPDATE v_user_insert
SET
name = 'jackView'
WHERE password='view';

-- 4. 删除视图数据
DELETE FROM v_user_insert
WHERE name = 'jackView';
```

#### 3.2 DML -- 解决视图污染基表问题
视图在 INSERT 和 UPDATE 时可能增加/修改一些视图看不见的数据, 从而对基表产生数据污染.  

为了解决这个问题, 在创建视图时, 引入`WITH CHECK OPTION` 约束  

该约束表示, **通过视图所做的修改,必须在视图的可见范围内**.  

- 假设 INSERT , 新增的记录在视图仍可查看
- 假设 UPDATE , 修改后的结果必须能通过视图查看到

实现的效果:  
当视图进行 INSERT 和 UPDATE 操作时, 系统先检查,操作后的数据对视图是否可见, 如果视图看不到操作后的结果, 那么不允许该 DML 操作

```sql
-- 1. 新建带 check option的视图
CREATE OR REPLACE VIEW v_test1
AS
SELECT name,password,salary,gender
FROM myuser
WHERE gender='M'
WITH CHECK OPTION;

-- 2. 插入污染数据
INSERT INTO v_test1
(name,password,gender,salary)
VALUES 
('check1','123456','W',2542);

-- 3. 报错
ERROR 1369 (HY000): CHECK OPTION failed 'test11.v_test1'

-- 4. 去掉视图中 WITH CHECK OPTION 约束, 插入污染数据 , 不报错
```

#### 3.3 DML -- 禁用DML操作
> 使用视图,绝大部分情况,都是用来进行查询, 绝少数需要 DML 查询, 有对基表的数据污染问题 , 所以我们希望禁用视图的 DML 功能 -- 将基表设为 READ ONLY

```sql
-- 1. 新建带 read only的视图
CREATE OR REPLACE VIEW v_test2
AS
SELECT name,password,salary,gender
FROM myuser
WITH READ ONLY;

-- 2. 插入数据
INSERT INTO v_test1
(name,password,gender,salary)
VALUES 
('check1','123456','W',2542);

```
## 四. 删除视图
> 删除视图不会导致基表数据丢失, 对视图进行 DML 的 DELETE 操作, 会删除基表中的数据

视图仅是基于表的一个查询定义, 对视图的删除不会导致基表数据的丢失, 不会影响基表数据.  

```sql
DROP VIEW v_test2;
```
