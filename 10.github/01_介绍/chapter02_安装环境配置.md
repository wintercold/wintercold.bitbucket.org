## SSH密钥配置
> 配置后不需要总输入密码

1. 生成密钥 - -f指定位置和名称
```bash
ssh-keygen -t rsa -C "xxxx@gmail.com" -f ~/.ssh/test-github
```
2. 复制公钥  
将test-github.pub文件的内容添加到github的“SSH KEYS”里面


## 导入 .gitconfig
> 包含个人信息和别名配置

先进入`cd ~/.gitconfig`,再修改
```bash
[user]
	email = fan.easter@gmail.com
	name = EasterFan



[alias]
	ci = commit
	st = status
	hi = log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short
	pu = push origin master
```
