## 一. 冲突场景
> f1 和 f2 分支同一个文件的同一行被修改

![](assets/markdown-img-paste-20180130204510328.png)  
1. 在 master 分支上新建一个 file.md 文件
2. 第一行输入测试文字,commit
3. 新建 f1 f2分支
4. 跳到 f1 分支, 编辑第二行, commit
5. 跳到 f2 分支, 编辑第二行, commit
6. 将 f1 合并到master分支 , 成功
7. 将 f2 合并到 master 分支 , 冲突
8. 因为 f1 和 f2 都修改了第二行

## 二. 报错
合并 f1 ,成功
```bash
easter8@easter8-QTK5:~/Desktop/fzconflct/fzconflit$ git merge f1
Updating cb0fd9a..3cfccdc
Fast-forward
 file.md | 1 +
 1 file changed, 1 insertion(+)
```
合并 f2 ,冲突
```bash
Auto-merging file.md
CONFLICT (content): Merge conflict in file.md
Automatic merge failed; fix conflicts and then commit the result.
```
## 三. 解决方法
#### 1. 找到冲突文件
上面的报错已经告诉我们, file.md 文件出现了冲突, 我们也可以通过`git st`查看详细
```bash
init
<<<<<<< HEAD
i'm f1
=======
i'm f2
>>>>>>> f2
```
HEAD 是当前分支内容  
下面的是冲突分支的内容
#### 2. 手动修改 file.md 为:  
```bash
init
i'm f1
i'm f2
```
#### 3. 将修改后的文件加入暂存区, 标识该冲突已解决,然后 commit
```bash
git add file.md
git ci -m "fix merge conflict"
```
