## 一. 分支简介
> 一个分支就是一个指针 , 分支的合并就是指针的前移等变化

- Git分支  
![branch1](assets/branch1.png "branch1")
- Gitflow工作流  
![branch2](assets/branch2.png "branch2")

## 二. 冲突解决
#### 1. 冲突发生的原因  

1. 在不同分支上，修改同一个文件；
2. 不同的人，修改了同一个文件--本地和远程修改同一个文件；  
3. 不同的仓库，修改了同一个文件--电脑C盘、D盘修改同一文件；  
4. 冲突只在合并分支的时候才会发生--不合并的时候在潜伏； 

#### 2. 冲突的解决方法  
1. 发生冲突并不可怕，冲突的代码不会丢失；
2. 解决冲突，编辑冲突文件，重新提交，commit 时不要给 message；
3. 冲突信息的格式；

## 三. 分支命令
> 查看 切换 删除 本地 远程 合并 跟踪 stash操作 变基合并

#### 1. 查看分支
```bash
# 1. 普通查看 - 列出所有分支和当前所在的版本号
git branch -av

# 2. 查看已合并的分支
git branch --merged
git branch --no-merged

# 3. 列出远程合并的分支
git branch -r --merged

# 4. 取出远程 fdf 分支
git checkout -t origin/fdf

```

#### 2. 基本操作
```bash
# 1.创建
git branch branchname

# 2. 切换
git checkout branchname
# 2.2 创建 + 切换
git checkout -b branchname

# 3. 重命名分支
git branch -m old_name new_name

# 4. 本地分支推远程
git push origin f1

# 5. 删除本地和远程分支
git branch -d master
git push origin <space>:f1
git fetch -p

# 6. 合并
git merge f1
# 6.1 合并分支, 拒绝 fast forward, 产生合并 commit
git merge --no-ff

# 7. 本地分支 和 远程分支 的关联关系
# 7.1 查看本地分支和远程分支的关联关系
git remote show origin
# 7.1 新建一个本地分支, 然后跟踪关联远程分支
git branch --track [本地分支名] [远程连接名]/[分支名]
# 7.2 已有一个本地分支, 然后跟踪关联远程分支
git branch --set-upstream [本地分支名] [远程连接名]/[分支名]

# 8. 变基合并 -- 暂时不会

```

#### 3. stash 储藏操作
场景:  
在子分支还有内容**没 add 到暂存区**或**暂存区没 commit**, 想要切换到主分支时, 不能切换  
此时用 stash 暂时保存子分支内容, 使我完成到主分支的切换 
```bash
# 保存进度
git stash save "储藏备注"
# 查看 stash 列表
git stash list
# 选择要恢复的分支
git stash apply stash@{1}
# 完成该储藏的工作, 删除该条储藏
git stash drop stash@{id}
# 弹出进度 - 从主分支操作完毕, 回到子分支, 用 pop 恢复 stash 暂存的内容
git stash pop 
# 删除 stash 列表 - 一般 pop 后, stash 暂存区就弹出,已经清空了
git stash clear
```
