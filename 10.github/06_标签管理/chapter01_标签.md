# 添加标签
```bash
#当前提交添加标签：
	git tag v1.0

#历史提交添加标签：
	 git tag v2.0 哈希值

#给标签添加注释:
	git tag v3.0 –m “message”

#在当前提交之前4个版本上，打标签v4.0：
	git tag v4.0 HEAD~4

#查看所有标签：
	git tag
```

# 删除标签
```bash
#删除本地标签：
	git tag –d v1.0

#删除远程标签：
	git push origin :refs/tags/v1.0

```
# 推送标签
```bash

#推送多个
	git push origin --tags

#推送一个
	git push origin v0.1
```
