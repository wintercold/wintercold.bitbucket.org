## 一. 完整流程
add的意义:  
add跟踪对文件的各种操作（删除、编辑、改名、移动），方便回撤 
```bash
# 1. 创建仓库
git init

    # 1.1 新建远程连接
git remote -v

    # 1.2 添加远程连接
git remote add origin https://github.com/Eas/Trygit.git

    #1.3 删除远程连接
git remote rm origin
    
git clone

# 2. add 保存工作区文件
git add .
    # 2.1 保存删除这一操作
git rm README.md

    # 2.2 一个文件多次提交(s 和 y),一个文件中对多处代码修改，将这几个修改分不同次提交----方便回撤
git add -p file

    # 2.3 查看是否正确分组
git diff 

    # 2.4 然后分多次 commit

# 3. commit 提交已保存的文件
git commit –m [message]

    # 3.1 直接从工作区提交到仓库 前提该文件已经有仓库中的历史版本
git commit –am [message]

# 4. 同步远程仓库
git push origin master
git pull origin master
```


## 二. 查看信息

#### 1.查看仓库 commit 历史
```bash
# 查看最近5次commit
git hi -5    首选
gitk   图形查看，更直观，无哈希值
```

#### 2.查看单个文件 commmit 历史
```bash
# 1. 推荐 图形查看，更直观，详细
gitk 
# 2. 简洁查看每个文件 commit  
git hi README.md    
```

#### 3. 过滤 commit 历史
```bash
# 1. 推荐 图形查看，更直观，详细
gitk 
# 2. 只查看 commit 中含有 add 的提交   
git hi --grep add    
```
#### 4. 查看单个文件在三个状态下的改变 -- diff
提交前要查看一下差异  
![diff](assets/diff.png "一图解千言")

## 三. 回撤
#### 1. 回撤的目的:  
  - 更改commit的Message
  - 将10次提交合并成3次提交
  - 将3次提交拆分成10次提交  

```bash
# 1. 用这一次提交覆盖上一次提交 - 更改 commit 的 message
  git commit --amend –m “message”

# 2. 变基操作，回撤上n次提交 
	git rebase –i HEAD~3
```

#### 2. 三个状态下的回撤
![reset](assets/reset.png "reset")
