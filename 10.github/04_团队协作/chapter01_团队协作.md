## 推送工作
> 本地仓库与远程仓库不同步 --push 失败 -- push 前先(pull/fetch)检查一下队友的工作

```bash
# 1. 提交前查看服务器上发生的改变 -- 推荐 gitk 图形界面查看队友提交日志
git log --no-merges origin master

# 抓取远程数据, 不影响本地工作区
git fetch origin

# 查看完毕后, 在本地 master 分支上进行合并, 将远程 master 分支合并到本地 master 分支
git checkout master
git merge origin/master

```
**git pull = git fetch + git merge**
